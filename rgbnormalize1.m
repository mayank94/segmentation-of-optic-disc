% imgdir='RimOneR3_ImTrain';
imgdir = 'rimoneimgs/cropped';

% imglist=dir([imgdir '/*.jpg']);
imglist = dir([imgdir '/*.png']);

refranges = [ 28  237;    5  170  ;  0   80]; % r; g; b

dref = refranges(:,2)-refranges(:,1);

for ii=1:numel(imglist)
    
    im=imread([imgdir filesep imglist(ii).name]);
    
    r=double(im(:,:,1));
    mM=minmax(r(:)');
    slope_r = dref(1)/(mM(2)-mM(1));
    intr = refranges(1,1) - slope_r * mM(1);
    
    r2 = uint8(slope_r * r + intr);
    
    g=double(im(:,:,2));
    mM=minmax(g(:)');
    slope_g = dref(2)/(mM(2)-mM(1));
    intg = refranges(2,1) - slope_g * mM(1);
    
    g2 = uint8(slope_g * g + intg);
    
    b=double(im(:,:,3));
    mM=minmax(b(:)');
    slope_b = dref(3)/(mM(2)-mM(1));
    intb = refranges(3,1) - slope_b * mM(1);
    
    b2 = uint8(slope_b * b + intb);
    
    trans = cat(3,r2,g2,b2);
    
%     figure(1)
%     subplot(1,2,1),imshow(im)
%     subplot(1,2,2),imshow(trans)
%     title(sprintf('%.3f %.3f %.3f',slope_r, slope_g, slope_b))
    
    imwrite(trans,['trans1/' imglist(ii).name]);
    
end
