load DispSectorFeatures;
load LabelVector;
load ConfidenceMap;
Features = horzcat(imfea,ConfVector_Disparity20);
lab=[];score=[];count(4) = 0;

classifier = TreeBagger(100,Features,labels,'OOBVarImp','on');
jap = classifier.OOBPermutedVarDeltaError;
[x,idx]=find(jap>0);
rel_features = Features(1:20,idx);

% for i = 1:size(idx,2)
%     if idx(i)<=64
%         count(1) = count(1)+1;
%     elseif idx(i)>64 && idx(i)<=128
%         count(2) = count(2)+1;
%     elseif idx(i)>128 && idx(i)<=192
%         count(3) = count(3)+1;
%     else
%         count(4) = count(4)+1;
%     end
% end

for i=1:20
    test = rel_features(i,:);
    train = rel_features;
    train(i,:) = [];
    train_labels = labels;
    train_labels(i) = [];
    classifier = TreeBagger(50,train,train_labels);
    [o1,o2] = predict(classifier,test);
    lab = vertcat(lab,o1);score = vertcat(score,o2);
end
[X,Y,T,AUC] = perfcurve(labels,score(:,2),'1');
% plot(X,Y);title('ROC Curves');xlabel('1-Specificity');ylabel('Sensitivity');legend(num2str(AUC));

load FeaturesVector
Features = horzcat(imfea,FeaturesVector);
lab=[];score=[];count(4) = 0;

classifier = TreeBagger(100,Features,labels,'OOBVarImp','on');
jap = classifier.OOBPermutedVarDeltaError;
[x,idx]=find(jap>0);
rel_features = Features(1:20,idx);
% 
% % for i = 1:size(idx,2)
% %     if idx(i)<=64
% %         count(1) = count(1)+1;
% %     elseif idx(i)>64 && idx(i)<=128
% %         count(2) = count(2)+1;
% %     elseif idx(i)>128 && idx(i)<=192
% %         count(3) = count(3)+1;
% %     else
% %         count(4) = count(4)+1;
% %     end
% % end
% 
for i=1:20
    test = rel_features(i,:);
    train = rel_features;
    train(i,:) = [];
    train_labels = labels;
    train_labels(i) = [];
    classifier = TreeBagger(50,train,train_labels);
    [o1,o2] = predict(classifier,test);
    lab = vertcat(lab,o1);score = vertcat(score,o2);
end
[X2,Y2,T2,AUC2] = perfcurve(labels,score(:,2),'1');

plot(X,Y,X2,Y2);title('ROC Curves');xlabel('1-Specificity');ylabel('Sensitivity');legend(num2str(AUC),num2str(AUC2));
