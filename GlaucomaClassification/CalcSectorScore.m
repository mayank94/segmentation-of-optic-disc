function [ConfidenceVector ConfImg] = CalcSectorScore(l_pred, GT_od, imroi, r, theta, Ridx, Ncells)

%% l_pred : predicted cup confidence superpixel image
%% GT_od: ground truth optic disc
%% imroi: Original image ROI
%% r: radius (eg: 0:0.001:10)
%% theta (eq: 0:0.001:pi)
%% Ridx: vector of fractions at which at which circles are required (eg: [0 0.25 0.4 0.65 1])
%% Ncells: number of cells required (eg: 16)

        S_ang = Ncells/(length(Ridx)-1);
        X1 = zeros(S_ang,1);
        X2 = zeros(S_ang,1);
        ConfidenceVector = zeros(1,Ncells);
        [odb2 odb1] = find(GT_od);
        odcenter = mean([odb1 odb2]);
        profile_im = GetPolarPlot_02_03_15(r, theta, 0, imroi(:,:,2), odcenter);
        [max1 ~] = max(max(profile_im(:,1:(length(theta)/2))));
        [max2 ~] = max(max(profile_im(:,(1+(length(theta))/2):(length(theta)))));
        if max2 > max1
            l_pred = fliplr(l_pred);
            GT_od = fliplr(GT_od);
            imroi = fliplr(imroi(:,:,2));
        end
        [odb2 odb1] = find(GT_od);
        odcenter = mean([odb1 odb2]);
        profile_od = GetPolarPlot_02_03_15(r, theta, 0, GT_od, [odcenter(2) odcenter(1)]);
        
            for m = 1 : S_ang
                X1(m) = 2*(m-1)*(pi/S_ang);
                X2(m) = 2*(m*(pi/S_ang));
            end
%             X1 = [    1           1         odcenter(2)+1    odcenter(2)+1];
%             Y1 = [    1       odcenter(1)+1       1          odcenter(1)+1];
%             X2 = [odcenter(2) odcenter(2)  size(GT_od,1)   size(GT_od,1)];
%             Y2 = [odcenter(1) size(GT_od,2) odcenter(1)    size(GT_od,2)];
%         else
%             for m = 1 : S_ang
%                 X1(m) = -2*m*(pi/S_ang);
%                 X2(m) = -2*((m-1)*(pi/S_ang));
%             end
%             X1 = [    1           1         odcenter(2)+1    odcenter(2)+1];
%             Y1 = [odcenter(1)+1   1         odcenter(1)+1           1       ];
%             X2 = [odcenter(2) odcenter(2)  size(GT_od,1)   size(GT_od,1)];
%             Y2 = [size(GT_od,2) odcenter(1) size(GT_od,2)    odcenter(1) ];
%         end
     %% Vertical OD radius
     profile_od = profile_od > 0;
     Rvert = (sum(profile_od(:,1)));
     Rmax = max(sum(profile_od,1));
%      figure, imshow(imroi)
%      hold on
%     figure, imshow(l_pred)
%     hold on
     %% To get a circular mask
     ODmask = zeros(size(GT_od));
     n = 1;
     ConfImg = zeros(size(GT_od));
     for j = 1:length(Ridx)-2
%          [yy xx] = meshgrid(odcenter(1) - (1:size(l_pred,2)), odcenter(2) - (1:size(l_pred,1)));
%          ODannulus = (hypot(xx,yy) < (j*Rmax)) - ODmask;
%          ODmask = hypot(xx,yy) < (j*Rmax);
%          [r2 r1]= find(bwperim(ODmask));
%          plot(r1, r2, 'r.')
%           imshow(ODannulus)   
         %% To get a sector
         for k = 1:S_ang
             theta = X1(k):0.001:X2(k);
%              profile_rtheta = GetPolarPlot_02_03_15(r, theta, 0, ODannulus, [odcenter(2) odcenter(1)]);
             ODsector = zeros(size(l_pred));
             Ax = [odcenter(1) odcenter(2) length(r)];
             for a = 1:length(theta)
                for b = round(Ridx(j)*Rvert)+1:round(Ridx(j+1)*Rvert)
                    ODsector(round(odcenter(2) + (r(b)*cos(theta(a)))),round(odcenter(1) + (r(b)*sin(theta(a))))) = GT_od(round(odcenter(2) + (r(b)*cos(theta(a)))),round(odcenter(1) + (r(b)*sin(theta(a))))); 
                end
            end
%              ODsector = zeros(size(ODannulus));
%              ODsector(X1(k): X2(k),Y1(k):Y2(k)) = ODannulus(X1(k): X2(k),Y1(k):Y2(k));
%              ODsector = ODsector > 0;
%              figure, imshow(ODsector,[])
             ODsector1 = ODsector.*l_pred;
             ConfImg(ODsector > 0) = mean(ODsector1(find(ODsector > 0)));
             ConfidenceVector(n) = mean(ODsector1(find(ODsector > 0)));
             n = n+1;
%              figure, imshow(ODsector)
%                 [s2 s1] = find(bwperim(ODsector1));
%                 plot(s1,s2,'b.', 'MarkerSize', 0.5);
%                 hold on
             
         end
     end
%      [od2 od1] = find(bwperim(GT_od));
%      plot(od1, od2,'b.')
     ODannulus = GT_od - ODmask;
%      figure, imshow(ODannulus)
%      for k = 1:4
%              ODsector = zeros(size(ODannulus));
%              ODsector(X1(k): X2(k),Y1(k):Y2(k)) = ODannulus(X1(k): X2(k),Y1(k):Y2(k));
       for k = 1:S_ang
             theta = X1(k):0.001:X2(k);
%              profile_rtheta = GetPolarPlot_02_03_15(r, theta, 0, ODannulus, [odcenter(2) odcenter(1)]);
             ODsector = zeros(size(ODannulus));
             for a = 1:length(theta)
                for b = round(Ridx(j+1)*Rvert)+1:round(Rmax)
                    ODsector(round(odcenter(2) + (r(b)*cos(theta(a)))),round(odcenter(1) + (r(b)*sin(theta(a))))) = GT_od(round(odcenter(2) + (r(b)*cos(theta(a)))),round(odcenter(1) + (r(b)*sin(theta(a))))); 
                end
             end
%              ODsector = zeros(size(ODannulus));
%              ODsector(X1(k): X2(k),Y1(k):Y2(k)) = ODannulus(X1(k): X2(k),Y1(k):Y2(k));
             ODsector = ODsector > 0; 
             ODsector1 = ODsector.*l_pred;
             ConfImg(ODsector > 0) = mean(ODsector1(find(ODsector > 0)));
             ConfidenceVector(n) = mean(ODsector1(find(ODsector > 0)));
             n = n+1;
%              [s2 s1] = find(bwperim(ODsector));
%                 plot(s1,s2,'b.', 'MarkerSize', 0.5);
%                 hold on
       end
%              figure, imshow(ConfImg)
       
%        profile_im = GetPolarPlot_02_03_15(r, theta, 0, imroi(:,:,2), odcenter);
%         [max1 ~] = max(max(profile_im(:,1:(length(theta)/2))));
%         [max2 ~] = max(max(profile_im(:,(1+(length(theta))/2):(length(theta)))));
%         odcenter = round(odcenter);
%         if max2>max1
%             temp = ConfidenceVector;
%             for n = 1:Ncells
%                 ConfidenceVector(Ncells - n+1) = temp(n);
%             end
%         end
end