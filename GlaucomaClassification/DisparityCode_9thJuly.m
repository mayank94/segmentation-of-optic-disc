%% Final code for disparity estimation - 09.07.15

addpath('ICP\');
load('RectValues.mat');

dir_img = dir('EightImages');
imgpath = 'EightImages\';
discpath = 'DiscImages\';
Avg_Rmse = zeros(8,1);
r = 1:250;
theta = 0:0.01:2*pi;
Ridx = [0 0.2 0.5 0.7 1];
Ncells = 64;
FeaturesVector = [];

for i = 1:size(dir_img)-2
    fname = dir_img(i + 2).name;
    fname1 = fname(1:end - 4);
    fnamenew = dir_img(i + 3).name;
    fnamenew1 = fnamenew(1:end - 4);
    I = imread(sprintf('%s%s', imgpath, fname));
    disc = imread(sprintf('%s%s%s', discpath, fname1, '-Disc-Avg.png'));
    load(sprintf('%s%s', fname1, '_poc.mat'));
    
%     [odb2 odb1] = find(disc > 0);
%     odcenter = mean([odb1 odb2]);
%     profile_od = GetPolarPlot_02_03_15(r, theta, 0, disc, [odcenter(2) odcenter(1)]);
%     profile_od = profile_od > 0;
%     Rmax = max(sum(profile_od,1));
%     rect = [odcenter(1)-(Rmax*1.5 + 20), odcenter(2)-(Rmax*1.5 +20), (3*Rmax +40), (3*Rmax + 40)];
% %     dROI = imcrop(D, rect);
%     disc = imcrop(disc, rect);
%     discROI = imcrop(disc, rect);
    
    disc = disc > 0;
    
%     input_points1 = input_points;
%     load(sprintf('%s%s', fnamenew1, '_poc.mat'));
%     base_points1 = base_points;
% I = imread('D:\Akshaya1\GlaucomaImplementation\RIM-ONE r3\DepthImages\S-31-L.jpg');
% disc = imread('D:\Akshaya1\GlaucomaImplementation\RIM-ONE r3\DepthImages\S-31-L-Disc-Avg.png');

Irgb1 = imcrop(I, rect1);
Irgb2 = imcrop(I, rect2);
% Irgb1 = imcrop(Irgb1, rect);
% Irgb2 = imcrop(Irgb2, rect);
Idisc = imcrop(disc, rect1);
Idisc = Idisc > 0;
Is1 = Irgb1(:,:,2);
Is2 = Irgb2(1:end - 1,1:end-2,2);

% cpselect(Is1, Is2);

% Is1 = Is1 - min(Is1(:));
% Is1 = Is1/max(Is1(:));
% Is2 = Is2 - min(Is2(:));
% Is2 = Is2/max(Is2(:));

%% Control points selection and adjustment
%cpselect(Is1, Is2);
input_points = cpcorr(input_points, base_points, Is1, Is2);
k = size(input_points, 1);

%% ICP
[TR, TT, ER, t] = icp([input_points zeros(k,1)]',[base_points, zeros(k,1)]');
% tmat = [1 0;0 1; 0 TT(2,1)*-1];

%% gives better output
tmat = [TR(1,1) TR(1,2);TR(2,1) TR(2,2); TT(1,1)*-1 TT(2,1)*-1]; 
T = maketform('affine', double(tmat));
B = imtransform(Is1,T,'XData',[1 size(Is2,2)],'YData',[1 size(Is2,1)]);
B1 = double(B);
% B1 = double(Is1);
% B1 = adapthisteq(B);
B1 = B1 - min(B1(:));
B1 = B1/max(B1(:));
B2 = double(Is2);
% B2 = adapthisteq(Is2);
B2 = B2 - min(B2(:));
B2 = B2/max(B2(:));

%cvexShowMatches(B1, B2, input_points, base_points);
D = disparity(B1,B2, 'BlockSize', 65 ,'DisparityRange', [-16 16], 'UniquenessThreshold', 0);
[d2 d1] = find(bwperim(Idisc));
% cvexShowDisparity(D);
% colormap gray
% hold on, plot(d1, d2, 'r.')

% base_points_new = [tmat(1,1)*input_points(:,1) + tmat(1,2)*input_points(:,2) + tmat(3,1); tmat(2,1)*input_points(:,1) + tmat(2,2)*input_points(:,2) + tmat(3,2)];
base_points_new = [tmat(1,1)*input_points(:,1) + tmat(1,2)*input_points(:,2) + tmat(3,1) tmat(2,1)*input_points(:,1) + tmat(2,2)*input_points(:,2) + tmat(3,2)];
G1 = base_points_new - base_points;
G2 = G1.*G1;
G3 = G2(:,1) + G2(:,2);
G4 = sqrt(G3);
Avg_Rmse (i) = sum(G4)/k;

    [odb2 odb1] = find(disc > 0);
    odcenter = mean([odb1 odb2]);
    profile_od = GetPolarPlot_02_03_15(r, theta, 0, disc, [odcenter(2) odcenter(1)]);
    profile_od = profile_od > 0;
    Rmax = max(sum(profile_od,1));
    rect = [odcenter(1)-(Rmax*1.5 + 20), odcenter(2)-(Rmax*1.5 +20), (3*Rmax +40), (3*Rmax + 40)];
    imROI = imcrop(Irgb1, rect);
    dROI = imcrop(D, rect);
%     disc = imcrop(disc, rect);
    discROI = imcrop(disc, rect);

    
    dROI = dROI.*discROI;
    [ConfidenceVector, ConfImg] = CalcSectorScore(double(dROI), discROI, imROI, r, theta, Ridx, Ncells);
    FeaturesVector = vertcat(FeaturesVector,ConfidenceVector);
%     dROI(~discROI) = 0;
    %figure, imshow(dROI,[])
    %figure, imshow(ConfImg,[])
    %save(strcat(fname,'_poc.mat'),'base_points','input_points');
end
