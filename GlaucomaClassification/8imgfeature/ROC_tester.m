load FeaturesVector;
load LabelVector;
lab=[];score=[];

for i=1:8
    test = FeaturesVector(i,:);
    train = FeaturesVector;
    train(i,:) = [];
    train_labels = labels;
    train_labels(i) = [];
    classifier = TreeBagger(1000,train,train_labels);
    [o1,o2] = predict(classifier,test);
    lab = vertcat(lab,o1);score = vertcat(score,o2);
end
[X,Y,T,AUC] = perfcurve(labels,score(:,2),'1');
plot(X,Y);title('Classifier ROC');xlabel('1-Specificity');ylabel('Sensitivity');
AUC   