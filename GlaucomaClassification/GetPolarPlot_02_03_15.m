function profile_rtheta = GetPolarPlot_02_03_15(r, theta, offset, I, GT_odcenter)

profile_rtheta = zeros(length(theta),length(r));
k=1;
if GT_odcenter == 0
    GT_odcenter = [301 301];
end
for a = 1:length(theta)
    for b = 1:length(r)
        x(k) = GT_odcenter(1) + (r(b)*cos(theta(a)+offset));
        y(k)= GT_odcenter(2) + (r(b)*sin(theta(a)+offset));
        k=k+1;
%         profile_rtheta(a,b) = interp2(I(:,:,2), y, x);
%         profile_rtheta(a,b) = GT_rim(round(x), round(y));
    end
end
prf = interp2(double(I), y, x);
profile_rtheta = reshape(prf,length(r), length(theta));
