function [ cropped_img ] = cropper( I,finres )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

[m,n,~] = size(I);

x = finres(6)*m/finres(16);
y = finres(7)*n/finres(17);

dim = 2*finres(11)*m/finres(16);
x = x-dim/2;
y = y-dim/2;
width = dim;
height = dim;

x1 = x;
y1 = y;

if x1<0
    x=0;
end

if y1<0;
    y=0;
end

if (x1+width)>n
    width=n-x1;
end

if (y1+height)>m
    height=m-y1;
end

rect = fix([x-10,y-10,width+10,height+10]);

cropped_img = imcrop(I,rect);

end

