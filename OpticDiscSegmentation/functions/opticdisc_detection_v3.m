% function [candidate,regcent,centval,conn,vmap,ch,twin,ell,params,majp,minp,linearea,majpar,minpar,flag1]= opticdisc_detection_v3(im,fm)
function [mask,centval, centmac,finres,varargout]=opticdisc_detection_v3(im,fm)
warning off
green_org=im(:,:,2);

origsiz = size(green_org);
factor=fix(origsiz(1)/256);

imr = imresize(im,1/factor,'bilinear');
rd = imr(:,:,1);
gr = imr(:,:,2);
bl = imr(:,:,3);

fmask1=imresize(fm,size(rd));
fmask1=fmask1>0;
fmask1=im2bw(rd,0.1);
fmask2=false(size(fmask1)+30);
fmask2(16:end-15,16:end-15)=fmask1;

% fmask1=imerode(fmask1,strel('square',15));
fmask2 = imerode(fmask2, strel('square',15));
fmask1 = fmask2(16:end-15,16:end-15);

mix=0.5*(double(imr(:,:,2))+double(imr(:,:,3)));

green2=adapthisteq(uint8(mix),'NumTiles',[16,16]);
S=Shade_Correction_fun_modified_od(im2double(green2),~fmask1,1,1);
S=S-min(S(:));
S=S./max(S(:));
rec_im=bloodvessels_morph_od(im2uint8(S),fmask1,0);
 
mv=max(rec_im(:));
[hist1,cen] = hist(double(rec_im(fmask1)),[0:2:255]);
hist2=cumsum(fliplr(hist1))/sum(hist1);
x1 = find(hist2 > 0.05, 1, 'first');
x1 = cen(end-x1)+1;

Ves= rec_im > x1;
Ves=Ves.*fmask1;
vess = Ves;
[gc,or,va]=gabor_master(mat2gray(imcomplement(S)),12);
ngc= zeros(size(gc));
for ik = 1:size(ngc,3)
    tmp = gc(:,:,ik);
    mn =min(tmp(:));
    mx=max(tmp(:));
    ngc(:,:,ik)=(gc(:,:,ik)-mn)./(mx-mn);
end
maxresp=max(ngc,[],3);
rec_im1=maxresp;
lims=stretchlim(rec_im1,[0 0.92]);
gab=im2bw(rec_im1,lims(2));
if sum(sum(vess(fix(size(vess,1)/2):end,:)))<=5
    vess(fix(size(vess,1)/2):end,:)=gab(fix(size(vess,1)/2):end,:);
end
vesmap=vess&gab;
% figure(1),subplot(3,4,3),imshow(rec_im,[]);

[acsis,parameter,linimage, linparams,hm,mv,rightval]=axis_determine_ransac(vess);

% ellipse parameters
e_ang = parameter(3);
slope = tan(parameter(3));
x0 = parameter(1)*500;
y0 = parameter(2)*500; % M1
aval=parameter(4)*500;
bval=parameter(5)*500;

finres = horzcat(x0,y0,aval,bval,e_ang);
[vmap,maxpt,linearea,flag1]=symmetric_vmap_band(rec_im,vesmap,acsis,fmask1,linimage,linparams,hm,mv,rightval);
par_skeleton=bwmorph(vmap,'skel','Inf');
skeleton=bwmorph(vess,'skel','Inf');
window=[maxpt(1)-eps,maxpt(1)+eps,maxpt(2)-eps,maxpt(2)+eps];

D1y = maxpt(2);
D1x = maxpt(1);
% figure(3),imshow(vmap)
% hold on
% plot(y0,x0,'ro')
% plot(D1y,D1x,'rs')
% hold off
[twin,majpar,minpar,majp,minp,miscmaj,miscmin]=twinparabola_fitting_module_v2(par_skeleton,skeleton,window);
% if majp(3)<0
%     window=[majp(1)*500-eps,majp(1)*500+eps,majp(2)*500+2*(majp(4)*500),majp(2)*500+6*(majp(4)*500)];
% else
%     window=[majp(1)*500-eps,majp(1)*500+eps,majp(2)*500-2*(majp(4)*500),majp(2)*500-6*(majp(4)*500)];
% end
if nargout>3
	varargout{1}=vmap;
	varargout{2}=majp;
	varargout{3}=minp;
end
px = majp(1)*500;
py = majp(2)*500;
p_ang = majp(3);
p_a = majp(4)*500;
finres = horzcat(finres,D1x,D1y,px,py,p_ang,p_a);
% figure(3),hold on
% plot(py,px,'r*')
% hold off

[ell,params,misce]=ellipse_fitting_module_v2(par_skeleton,majp);



n_inlier_par = sum(sum(twin & vmap>0));
n_inlier_ell = sum(sum(ell & vmap>0));

den = sum(vmap(:));
flag1 = n_inlier_par/den;  % >0.1 maximize
flag2 = n_inlier_ell/den ; % >0.15 maxim

[Y,X]=meshgrid(1:size(vess,2),1:size(vess,1));
allpts = [X(:),Y(:)];
[~,points_maj] = parabola(majp,allpts./500);
[~,points_min] = parabola(minp, allpts./500);

if true % new set of conditions for fit reliability
    tmp1 = zeros(size(vess));
    tmp2 = tmp1;
    tmp1(:) = points_maj;
    tmp2(:) = points_min;
    res_par = abs(tmp1)+abs(tmp2);
    n_inlier_par = sum(res_par(vmap>0));
    
    flag1 = n_inlier_par/den; % <0.15] %minimize
    [~,res_ell] = ellipse(params, allpts./500);
    res_ellip = zeros(size(vess));
    res_ellip(:)=abs(res_ell);
    
    n_inlier_ell = sum(res_ellip(vmap>0));
    flag2 = n_inlier_ell/den; % <0.1 % minimize
end

outfig2=zeros(size(vess));
outfig2(:)=min(points_maj,points_min);
% outfig2=outfig2./max(outfig2(:));
vess_par = outfig2(vess>0);

flag3 = mean(abs(outfig2(vess>0))); %<0.03; (weak) % should be small, close to 0.002

flag3b = sum(vess_par>=0)/sum(vess_par<0); %<1 should be low (more negative vess pixels)

flag3c = -mean(vess_par(vess_par>=0))/mean(vess_par(vess_par<0)); %<1

c1 = sum(outfig2(vess>0&twin))/sum(outfig2(vess>0));

[~,points_ell] = ellipse(params,allpts./500);

outfig3 = zeros(size(vess>0));
outfig3(:) = points_ell;
% outfig3 = outfig3./max(outfig3(:));
vess_ell = outfig3(vess>0);

flag4 = mean(abs(outfig3(vess>0))); % < 0.015; (weak) % should be small, close to 0.002
flag4b = sum(vess_ell>0)/sum(vess_ell<0); % <2.3 should be low (not too many positives)
flag4c = -mean(vess_ell(vess_ell>=0))/mean(vess_ell(vess_ell<0)); % <2.5

c2 = sum(outfig3(vess>0 & twin)) / sum(outfig3(vess>0));

% [flag]=ellpar_fit_checking(majp,params,miscmaj,miscmin,misce);

flag = flag1>0.1 & flag2>0.1 & flag3b < 1 & flag3c < 1 ; %& ...
    %flag4b < 2.3 & flag4c < 2.5;
if true
    
    flag = flag1<0.15 & flag2 < 0.05; % & flag3b < 1 & flag3c < 1;
    
end

%intbox=make_box(majp(1)*500,majp(2)*500,fix(size(rd,1)/3),rd);
intbox=make_box(D1x,D1y,1.4*majp(4)*500,rd);

if flag
    intimage=false(size(rd));
    intimage(intbox(1):intbox(2),intbox(3):intbox(4))=1;
else
    intimage = true(size(rd));
end

cent=linspace(0,255,64);

hc(1,:) = hist(double(rd(fmask1)), cent);
hc(2,:) = hist(double(gr(fmask1)), cent);
hc(3,:) = hist(double(bl(fmask1)), cent);


% find mode
[mv,mi]=max(hc(:,2:end-1),[],2);
mi=mi+1;
vi = mv;
mi1=mi;

ti=zeros(1,3);
% find brightest 10%, past the mode from reverse
for i=1:3
    while vi(i)>mv(i)/5
        mi(i)=mi(i)+1;
        if mi(i)>=numel(cent)
            break
        end
        vi(i)=hc(i,mi(i));
    end
    hc_bright = hc(i,mi(i):end);
    cs = cumsum(hc_bright)/sum(hc_bright);
%     mi(i)
    bi_5 = find(cs>1-0.05,1,'first');
    if ~isempty(bi_5)
        ti(i)=bi_5;
    end
end
% q1=ti_b-mi_b

% cs=cumsum(hc(mi(2):end))/sum(hc(mi_g:end));
% ti(2) = find(cs>1-0.05,1,'first')
% % q2=ti_g-mi_g
% 
% cs=cumsum(hc(mi(3):end))/sum(hc(mi_r:end));
% ti(3) = find(cs>1-0.05,1,'first')
% % q3=ti_r-mi_r

[~,channelseq] = sort(ti,'descend');

bw1 = [];
selch = [];
green = [];

for sel=channelseq
    ch = imr(:,:,sel);
    hci=hc(sel,:);
    m20i = mi(sel);
%     tii = ti(sel);
    csi=cumsum(hci(m20i:end));
    tii=find(csi/csi(end)>1-0.05,1,'first');
    hp5 = hci(m20i:m20i+tii-1);
    me = mean(cent(m20i+tii-1:end));%sum(cent(m20i:m20i+tii-1).*hp5)/sum(hp5);
    igamma=log(255.)/log(me);
    gamma=1/igamma;
    
    
%     gamma = mg/32    % 15 is working well for diaretdb1
    green = imadjust(ch-uint8(cent(m20i)),[0,1],[0,1],gamma);
    hgi=hist(double(green(fmask1)),cent);
    csi=cumsum(hgi); 
    t95i=find(csi/csi(end)>1-0.01,1,'first');
    if cent(t95i)<10   
        che = adapthisteq(ch);
        hci = hist(double(che(fmask1)),cent);    
        [mvi,mii]=max(hci(2:end-1));
        mii=mii+1;
        vi = mvi;
        m20i = mii;
        while vi>mvi/5
            m20i=m20i+1;
            if m20i>=numel(cent)
                break
            end
            vi=hci(m20i);
        end

        csi=cumsum(hci(m20i:end));
        tii=find(csi/csi(end)>1-0.05,1,'first');
        hp5 = hci(m20i:m20i+tii-1);
        me = sum(cent(m20i:m20i+tii-1).*hp5)/sum(hp5);
        igamma=log(255.)/log(me);
        gamma=1/igamma;
        green=imadjust(ch-uint8(cent(m20i)),[0,1],[0,1],gamma);
    end   
    green=im2double(green);
    threshold=graythresh(green(intimage));
    greenimage=green.*intimage;

    bw1 = im2bw(greenimage,threshold);
    
    % TODO: remove bwcomponents of bw1 which are big and large part of perimeter touch the fundus
    % mask
    % threshold
    limit=sum(bw1(:))/sum(sum(fmask1&intimage)); 

    if limit > 0.2 || limit < 0.008 %limit == 0
        continue
    else 
        selch = ch;        
        break
    end

end

if isempty(selch)
%     selch = imr(:,:,2);
    bw1 = zeros(size(green));
    candbox=make_box(majp(1)*500,majp(2)*500,fix(size(rd,1)/6),rd);
    candidate=zeros(size(rd));
    candidate(candbox(1):candbox(2),candbox(3):candbox(4))=1;
    regcent=[-1,-1];
    centval=[majp(1)*500,majp(2)*500];
    conn=bwconncomp(bw1);
    [centmacell,centmacpar]=mac_detection_pinpoint(gr,majp,params,vmap);
    [dm,~]=bwdist(vmap);
    centmacell = fix(centmacell);
    centmacpar = fix(centmacpar);
    finres = horzcat(finres,centmacell(1),centmacell(2),centmacpar(1),centmacpar(2),size(rd,1),size(rd,2));
    if dm(centmacell(1),centmacell(2))>dm(centmacpar(1),centmacpar(2))
        centmac = factor*centmacell ;   
    else
        centmac = factor*centmacpar;   
    end
    centval=centval*factor;
    mask = circmask(centval, size(im,1)/10,origsiz);
	
	if nargout>6
		varargout{4} = centmacell;
		varargout{5} = centmacpar;
	end
	if nargout > 8        
        varargout{6} = ell;
    end
    
    return
    
%     pause

end

if false
    figure(1),imshow(double(vesmap)+...
        double(twin)+...
        double(ell)+...
        double(intimage)+double(bw1),[]);
    hold on
    plot(majp(2)*500,majp(1)*500,'rs');
    hold off
end

[centmacell,centmacpar]=mac_detection_pinpoint(ch,majp,params,vmap);
[dm,~]=bwdist(vmap);
centmacell = fix(centmacell);
centmacpar = fix(centmacpar);
finres = horzcat(finres,centmacell(1),centmacell(2),centmacpar(1),centmacpar(2),size(rd,1),size(rd,2));
if dm(centmacell(1),centmacell(2))>dm(centmacpar(1),centmacpar(2))
    centmac = factor*centmacell ;   
else
    centmac = factor*centmacpar;   
end


if nargout>6
	varargout{4} = centmacell;
	varargout{5} = centmacpar;
end

if nargout>8
    varargout{6} = ell; 
end

% % figure(1),subplot(3,4,1),imshow(ch,[]);
out=bw1;%imdilate(bw1,strel('disk',5)); %gr3;
bw1 = bwmorph(out,'hbreak');
% tmp = bwmorph(out,'remove');
conn=bwconncomp(bw1,4);

rp=regionprops(conn,'MajorAxisLength','MinorAxisLength');
maj=[rp.MajorAxisLength]; %struct2cell(maj);
% maj=cell2mat(maj);
% minor=regionprops(conn,'MinorAxisLength');
% minor=struct2cell(minor);
% minor=cell2mat(minor);
minor=[rp.MinorAxisLength];
peri = bwmorph(bw1,'remove');

lm = labelmatrix(conn);

tmp1 = double(lm).*peri;
npfull = hist(tmp1(:),[0:conn.NumObjects]);
npfull = npfull(2:end);

tmp = double(lm).*(peri&~fmask1);
np = hist(tmp(:),[0:conn.NumObjects]);
np = np(2:end);


area = hist(double(lm(:)),[0:conn.NumObjects]);
area = area(2:end);

if ~isempty(maj)
    pass = np./npfull<0.2 & area>100 & (maj./minor)<5;
else
    pass = np./npfull<0.2 & area>100;
end


conn2 = conn;
conn2.PixelIdxList = conn.PixelIdxList(pass);
conn2.NumObjects = sum(pass);

conn = conn2;
lcon = labelmatrix(conn);

reg=lcon>0;
regd=imdilate(reg,strel('disk',5));
lcon=bwlabel(regd);
conn=bwconncomp(regd);

rl = lcon(Ves==1);
rl = union(rl,[]);
rl(rl==0)=[];

conn.NumObjects = numel(rl);
conn.PixelIdxList = conn.PixelIdxList(rl);
% lcom=labelmatrix(conn);

if conn.NumObjects==0
    out = od_entropy(imr,intimage);
    
    conn=bwconncomp(out);
    
    lcon = labelmatrix(conn);

    rl=lcon(Ves==1);
    rl = union(rl,[]);
    rl(rl==0)=[];

    conn.NumObjects = numel(rl);
    conn.PixelIdxList = conn.PixelIdxList(rl);
    lcom=labelmatrix(conn);
    if conn.NumObjects==0
        candbox=make_box(majp(1)*500,majp(2)*500,fix(size(rd,1)/6),rd);
        candidate=zeros(size(rd));
        candidate(candbox(1):candbox(2),candbox(3):candbox(4))=1;
        regcent=[-1,-1];
        centval=[majp(1)*500,majp(2)*500];
        centval=centval*factor;
        mask = circmask(centval, size(im,1)/10,origsiz);
        return;
    end
end

p=regionprops(conn,'MajorAxisLength','MinorAxisLength');
aspect_ratio = [p.MajorAxisLength]./[p.MinorAxisLength];
% q=struct2cell(p);
% s=cell2mat(q);
% aspect_ratio=[];
% for fi = 1:size(s,2)
%     aspect_ratio(fi)= s(1,fi)/s(2,fi);
% end

ar=aspect_ratio<5.5;
conn.NumObjects = sum(ar);
conn.PixelIdxList = conn.PixelIdxList(ar);


if conn.NumObjects==0
    candbox=make_box(majp(1)*500,majp(2)*500,fix(size(rd,1)/6),rd);
    candidate=zeros(size(rd));
    candidate(candbox(1):candbox(2),candbox(3):candbox(4))=1;
    regcent=[-1,-1];
    centval=[majp(1)*500,majp(2)*500];
    centval=centval*factor;
    mask = circmask(centval, size(im,1)/10,origsiz);
    return;
end

lfinal=labelmatrix(conn);
output=lfinal>0;

conn=bwconncomp(output);

%XXX: return without feature classification
out = output;
[label,nlabel]=bwlabel(out);
prop1=regionprops(label,'Centroid');
prop1=struct2cell(prop1);
prop1=cell2mat(prop1');
candidate=zeros(size(ch));
for i=1:nlabel
    centr=[prop1(i,2),prop1(i,1)];
    centor(i,:)=centr;    
    centrp=[majp(1)*500;majp(2)*500];
    distncep(i)=dist(centr,centrp);   
   
end

[mnvalp,mnposp]=min(distncep);
regcent=[fix(centor(mnposp,1)),fix(centor(mnposp,2))];

candidate(label==mnposp)=1;
chout1=im2double(ch).*candidate;
% [rreg,creg]=find(chout1==max(chout1(:)));
parval=[majp(1)*500,majp(2)*500];
% [centval1,greenodcent]=od_center_pinpoint(rec_im,fmask1,ch,regcent,parval,params);
% regmat=[rreg,creg];
% allmat=[parval;regmat];
% distmat=dist(centval1,allmat');
% [minval,minpt]=min(distmat);

if majp(3)<0
    upc=regcent(2)+20;
    if upc>size(ch,2)
        upc=size(ch,2);
    end
    rowvec=chout1(regcent(1),regcent(2)+10:upc);
    [~,mp]=max((rowvec));
    colval=regcent(2)+5;
else
    lpc=regcent(2)-20;
    if lpc<1
        lpc=1;
    end
    rowvec=chout1(regcent(1),lpc:regcent(2)-10);
    [~,mp]=max(rowvec);
    colval=regcent(2)-5;
end
centval=[regcent(1),colval]*factor;
mask = circmask(centval, size(im,1)/10,origsiz);
return;
