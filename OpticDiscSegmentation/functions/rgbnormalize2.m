function [im2] = rgbnormalize2(im)
% imglist=dir('trans1/*.png');
% 
refim = imread('rim-one-r1\Normal\Im001.bmp');
refim_hsv = rgb2hsv(refim);
ref_v = refim_hsv(:,:,3);

refhist = hist(ref_v(:),[0:1/128:1]);

% for ii=1:numel(imglist)
%     im=imread(['trans1\' imglist(ii).name]);
    im_hsv = rgb2hsv(im);
    [v2,T] = histeq(im_hsv(:,:,3),refhist);
    im2_hsv=im_hsv;
    im2_hsv(:,:,3)=v2;
    im2=hsv2rgb(im2_hsv);
%     figure(1)
%     subplot(1,2,1), imshow(im)
%     subplot(1,2,2), imshow(im2)
%     imwrite(im2,['trans2/' imglist(ii).name]);
% end
