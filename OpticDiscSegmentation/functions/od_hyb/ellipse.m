function [v,vs] = ellipse(args,pts)

pts_x = pts(:,1)-args(1);
pts_y = pts(:,2)-args(2);

before = [pts_x, pts_y]';



c = cos(args(3));
s = sin(args(3));


pts_rot = [c, -s; s, c]' * before;
xval=[ones(1,size(pts_rot,2))-(pts_rot(1,:).^2)/args(4)^2]*(args(5))^2;

v = (pts_rot(2,:)).^2 - xval;

vs = v';

v=abs(v)';

