clc;
% close all;
dirnames={'odfull_diaretdb1',...
'odfull_dmed','odfull_messidor',... 
'odfull_drive','odfull_sel_ou'};

dirnames1={'full_OD/diaretdb1_sel',...
'full_OD/dmed_sel','full_OD/messidor_sel',... 
'full_OD/drive_sel','full_OD/sel_out_80'};
load('randtreenvarall.mat');

for di=2:numel(dirnames)
    dirname = dirnames{di};
    dirname1= dirnames1{di};
%     xpng=dir([dirname '/*.png']);
%     xtif = dir([dirname '/*.tif']);
%     xjpg = dir([dirname '/*.jpg']);
%     x = cat(1,xpng,xtif,xjpg);
    x=dir([dirname '/*.mat']);
    outdir=['od_regions_sel_' dirname(8:end)];
    if ~exist(outdir,'dir')
        mkdir(outdir)
    end
    maxim=numel(x);
    if di==1 | di==5
        ext='png';
    elseif di==2
        ext='jpg';
    else
        ext='tif';
    end

    for u=1:maxim
        try
            load(sprintf('%s/%s',dirname,x(u).name));
            inp=imread(sprintf('%s/%s.%s',dirname1,x(u).name(9:(end-4)),ext));
        catch
            continue;
        end
        origsiz = [size(inp,1),size(inp,2)];
        factor=fix(origsiz(1)/256);
        
        imr = imresize(inp,1/factor);

        rd = imr(:,:,1);
        gr = imr(:,:,2);
        bl = imr(:,:,3);
        
        regions=zeros([size(rd),size(candid,3)+3]);
        
        regions(:,:,1)=imresize(eimg1,size(rd));
        regions(:,:,2)=imresize(eimg2,size(rd));
        regions(:,:,3)=imresize(pimg,size(rd));
        for t=1:size(candid,3)
            regions(:,:,3+t)=candid(:,:,t);
        end
        

        out=false(size(gr));

        fmask=im2bw(rd,0.1);

        fmask1=imerode(fmask,strel('disk',fix(25/factor)));


        cent=linspace(0,255,64);


        hc(1,:) = hist(double(rd(fmask1)), cent);
        hc(2,:) = hist(double(gr(fmask1)), cent);
        hc(3,:) = hist(double(bl(fmask1)), cent);


        % find mode
        [mv,mi]=max(hc(:,2:end-1),[],2);
        mi=mi+1;
        vi = mv;
        mi1=mi;

        ti=zeros(1,3);
        % find brightest 10%, past the mode from reverse
        for i=1:3
            while vi(i)>mv(i)/5
                mi(i)=mi(i)+1;
                if mi(i)>=numel(cent)
                    break
                end
                vi(i)=hc(i,mi(i));
            end
            hc_bright = hc(i,mi(i):end);
            cs = cumsum(hc_bright)/sum(hc_bright);
        %     mi(i)
            bi_5 = find(cs>1-0.05,1,'first');
            if ~isempty(bi_5)
                ti(i)=bi_5;
            end
        end

        [~,channelseq] = sort(ti,'descend');

        bw1 = [];
        selch = [];
        green = [];

        for sel=channelseq
            ch = imr(:,:,sel);
            hci=hc(sel,:);
            m20i = mi(sel);
        %     tii = ti(sel);
            csi=cumsum(hci(m20i:end));
            tii=find(csi/csi(end)>1-0.05,1,'first');
            hp5 = hci(m20i:m20i+tii-1);
            me = mean(cent(m20i+tii-1:end));%sum(cent(m20i:m20i+tii-1).*hp5)/sum(hp5);
            igamma=log(255.)/log(me);
            gamma=1/igamma;


        %     gamma = mg/32    % 15 is working well for diaretdb1
            green = imadjust(ch-uint8(cent(m20i)),[0,1],[0,1],gamma);
            hgi=hist(double(green(fmask1)),cent);
            csi=cumsum(hgi); 
            t95i=find(csi/csi(end)>1-0.01,1,'first');
            if cent(t95i)<10   
                che = adapthisteq(ch);
                hci = hist(double(che(fmask1)),cent);    
                [mvi,mii]=max(hci(2:end-1));
                mii=mii+1;
                vi = mvi;
                m20i = mii;
                while vi>mvi/5
                    m20i=m20i+1;
                    if m20i>=numel(cent)
                        break
                    end
                    vi=hci(m20i);
                end

                csi=cumsum(hci(m20i:end));
                tii=find(csi/csi(end)>1-0.05,1,'first');
                hp5 = hci(m20i:m20i+tii-1);
                me = sum(cent(m20i:m20i+tii-1).*hp5)/sum(hp5);
                igamma=log(255.)/log(me);
                gamma=1/igamma;
                green=imadjust(ch-uint8(cent(m20i)),[0,1],[0,1],gamma);
            end   

            threshold=graythresh(green(fmask1));

            bw1 = im2bw(green,threshold);


            limit=sum(bw1(:))/sum(fmask1(:)); %(size(green,1)*size(green,2));

            if limit > 0.2 || limit == 0
                continue
            else 
                selch = ch;        
                break
            end
        end

        mix=0.5*(double(gr)+double(bl)); 

        green2=adapthisteq(uint8(mix),'NumTiles',[16,16]);
        rec_im=bloodvessels_morph(green2,fmask1,0);

        mv=max(rec_im(:));
        [hist1,cen] = hist(double(rec_im(fmask1)),[0:2:255]);

        hist2=cumsum(fliplr(hist1))/sum(hist1);
        x1 = find(hist2 > 0.1, 1, 'first');
        x1 = cen(end-x1)+1;

        Ves= rec_im > x1;
        vess=Ves.*fmask1;
        features=[];
        features=zeros(size(regions,3),15);
        for numb=1:size(regions,3)
            finalimg=regions(:,:,numb);
            if sum(finalimg(:))==0
                continue;
            end
            features(numb,:) = feats_window_select(finalimg,ch,vess,imr);
        end
        if isempty(features)
            imwrite(inp,sprintf('%s/%s.png',outdir,x(u).name(9:(end-4))));
        end
        [lab,score]=predict(B,features);
        lab=cell2mat(lab);
        lab=str2num(lab);
        aray=find(lab>0);
        if numel(aray)==1
            lab_val=find(lab==1);
        else
            aray1=score(:,2);
            lab_val=find(aray1==max(aray1));
        end
        lablmat=bwlabel(finalimg);
        rgbim=inp;
        
        for ref=1:numel(aray)
             mask=zeros(size(rd));
             mask=regions(:,:,aray(ref));
             mask=imresize(mask,[size(inp,1),size(inp,2)]);
             mask=imdilate(bwmorph(mask,'remove'),strel('square',5));
             r=rgbim(:,:,1);
             g=rgbim(:,:,2);
             b=rgbim(:,:,3);
             r(mask)=255;
             g(mask)=255;
             b(mask)=0;
             rgbim=cat(3,r,g,b);
        end
        mask=[];
        
        arayfalse=setdiff(1:size(features,1),aray);
        for ref=1:numel(arayfalse)
             mask=zeros(size(rd));
             mask=regions(:,:,arayfalse(ref));
             mask=imresize(mask,[size(inp,1),size(inp,2)]);
             mask=imdilate(bwmorph(mask,'remove'),strel('square',5));
             r=rgbim(:,:,1);
             g=rgbim(:,:,2);
             b=rgbim(:,:,3);
             r(mask)=150;
             g(mask)=0;
             b(mask)=0;
             rgbim=cat(3,r,g,b);
        end
        mask=[];

%         reg_val=features*coefficients;
%         lab_val=find(reg_val==max(reg_val));

%         nl=setdiff(1:max(lablmat(:)),lab_val);
%         for ri=1:numel(nl)
%             finalimg(lablmat==nl(ri))=0;
%         end
        for ref=1:numel(lab_val)
             mask=zeros(size(rd));
             mask=regions(:,:,lab_val(ref));
             mask=imresize(mask,[size(inp,1),size(inp,2)]);
             mask=imdilate(bwmorph(mask,'remove'),strel('square',5));
             r=rgbim(:,:,1);
             g=rgbim(:,:,2);
             b=rgbim(:,:,3);
             r(mask)=0;
             g(mask)=200;
             b(mask)=0;
             rgbim=cat(3,r,g,b);
        end
        mask=[];


        rgbim=cat(3,r,g,b);
        mask=[];
        finimg=im2double(ch).*finalimg;
        imwrite(rgbim,sprintf('%s/%s.png',outdir,x(u).name(9:(end-4))));
%         save(sprintf('%s/%s.mat',outdir,x(u).name(1:(end-4))),'finalimg','ch','vess');
    end
end
