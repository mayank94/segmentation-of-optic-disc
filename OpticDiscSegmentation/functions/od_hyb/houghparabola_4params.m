function [Imbinary1,line_image,phi,p,para] = houghparabola_4params(Imbinary,centrox)%,centrox,centroy,pmin,pmax)
%HOUGHPARABOLA - detects parabola with specific vertex in a binary image.
%
%Comments:
%       Function uses Standard Hough Transform to detect parabola in a 
%       binary image. According to the Hough Transform, each pixel in image
%       space corresponds to a parabola in Hough space and vise versa. This
%       function uses the representation of parabola: 
%       [(y-centroy)*cos(phi)-(x-centrox)*sin(phi)]^2=...
%               ...=4*p*[(y-centroy)*sin(phi)+(x-centrox)*cos(phi)]
%       to detect parabola in binary image.
%       Upper left corner of image is the origin of coordinate system.
%
%Usage: [phi,p] = houghparabola(Imbinary,centrox,centroy,pmin,pmax)
%
%Arguments:
%       Imbinary - a binary image. image pixels that have value equal to 1 
%                  are interested pixels for HOUGHPARABOLA function.
%       centrox  - column coordinates of the parabola vertex.
%       centroy  - row coordinates of the parabola vertex.
%       pmin     - minimum possible value of the distance p between the 
%                  vertex and focus of the parabola.
%       pmax     - maximum possible value of the distance p between the 
%                  vertex and focus of the parabola.
%                  
%
%Returns:
%       phi      - angle of the detected parabola in polar coordinates
%       p        - distance between vertex and focus of the detected
%                  parabola.
%
%Written by :
%       Clara Isabel Snchez
%       Biomedical Engineering Group
%       ETS Ingenieros de Telecomunicaciones
%       University of Valladolid,Spain
%       csangut@gmail.com
%
%August 6,2007      - Original version

% erode the vessels so that only the vascular arch is left
Imbinary_copy = Imbinary; % to be used later for display
% disk = strel('disk',1,8);   
% Imbinary = imerode(Imbinary,disk);
% figure,imshow(Imbinary)
pmax=250;
pmin=0;
vector_p=linspace(-pmax,pmax);
vector_phi1=linspace(-10*(pi/180),10*(pi/180),50);
vector_phi2=linspace(170*(pi/180),190*(pi/180),50);
vector_phi=[vector_phi1,vector_phi2];
Accumulator = zeros([length(vector_phi),length(vector_p)]);
[y,x] = find(Imbinary);

% tic
% for i = 1:length(x)
%    for j= 1:length(vector_phi)
%        Y=y(i)-centroy;
%        X=x(i)-centrox;
%        angulo=vector_phi(j);
%        numerador=(Y*cos(angulo)-X*sin(angulo))^2;
%        denominador=4*(X*cos(angulo)+Y*sin(angulo));
%        if denominador~=0
%            p=numerador/denominador;
%            
%            if abs(p)>pmin && abs(p)<pmax && p~=0
%                indice=find(vector_p>=p);
%                indice=indice(1);
%                Accumulator(j,indice) = Accumulator(j,indice)+1;
%            end
%        end
%    end
% end
% toc


%Voting
% for i = 1:length(x)
tic
l=1;
for centroy=1:size(Imbinary,1)
    
   for j= 1:length(vector_phi)
       Y=y-centroy;
       X=x-centrox;
       angulo=vector_phi(j);
% % % %        numerador=(Y*cos(angulo)-X*sin(angulo)).^2;
% % % %        denominador=4*(X*cos(angulo)+Y*sin(angulo));
% % % %        ar=abs(denominador)>0;
% % % %        inf_pos=find(ar==0);
% % % %        p=numerador./denominador;      

        before = [X,Y]';
        c = cos(angulo);
        s = sin(angulo);

        pts_rot = [c, -s; s, c]' * before;

        num=(pts_rot(2,:)).^2 ;
        den=4*(pts_rot(1,:));
        p=num/den;
        ar=abs(den)>0;
       inf_pos=find(ar==0);
        centroy
        j
        p
       cond=abs(p)>pmin & abs(p)<pmax & abs(p)>0;
       req_idx=find(cond==1);
       cor_idx=setdiff(req_idx,inf_pos);
       pval=p(cor_idx);
       vectorp_mat=repmat(vector_p,numel(pval),1);
       pmat=repmat(pval,1,size(vector_p,2));
       submat=vectorp_mat-pmat;
       repval=max(submat(:))+100;
       submat(submat<0)=repval;
       minval=min(submat,[],2);
       repeat=repmat(minval,1,size(submat,2));
       submat1=submat-repeat;
       ansmat=zeros(size(submat));
       ansmat(submat1==0)=1;
       sum_array=sum(ansmat);
       indice=find(sum_array>0);
       Accumulator(j,indice) = sum_array(indice);           
       
   end
accumulator(:,:,centroy)=Accumulator;
maximo=max(max(Accumulator));
[idx_phi,idx_p]=find(Accumulator==maximo,1,'first');
p=vector_p(idx_p);
phi=vector_phi(idx_phi);
maximom(l)=maximo;
pval(l)=p;
phival(l)=phi;
l=l+1;
end
   toc
% end
vec_centroy=1:1:centroy;
% Finding local maxima in Accumulator
% figure,imagesc(Accumulator);
[mva,midx]=max(maximom);
p=pval(midx);
phi=phival(midx);
centroy=vec_centroy(midx);

[sva,sidx]=sort(accumulator(:),'descend');
for ol=1:5
[iphi,ip,icentroy]=ind2sub([size(accumulator)],sidx(ol));
sidxmat(ol,:)=[iphi,ip,icentroy];
end
sp=pval(sidxmat(:,2));
sphi=phival(sidxmat(:,1));
scentroy=vec_centroy(sidxmat(:,3));
%%
% draw the parabola
tic
parabola = zeros(size(Imbinary));
[X,Y]=meshgrid(1:size(parabola,2),1:size(parabola,1));
pts = [X(:),Y(:)];
phi=phi(1);
p=p(1);
val = ((pts(:,2)-centroy)*cos(phi)-(pts(:,1)-centrox)*sin(phi)).^2 - 4*p*((pts(:,2)-centroy)*sin(phi)+(pts(:,1)-centrox)*cos(phi));
parabola(:)=val;
parabola=parabola<1;
toc;

for ti=1:5
    sparabola = zeros(size(Imbinary));
    [X,Y]=meshgrid(1:size(parabola,2),1:size(parabola,1));
    pts = [X(:),Y(:)];
    phi=sphi(ti);
    p=sp(ti);
    centroy=scentroy(ti)
    pts_x=pts(:,1)-centrox;
    pts_y=pts(:,2)-centroy;
    before = [pts_x,pts_y]';
    c = cos(phi);
    s = sin(phi);

    pts_rot = [c, -s; s, c]' * before;

    val=(pts_rot(2,:)).^2-4*(-p)*(pts_rot(1,:));
   
%     val = ((pts(:,2)-centroy)*cos(phi)-(pts(:,1)-centrox)*sin(phi)).^2 - 4*p*((pts(:,2)-centroy)*sin(phi)+(pts(:,1)-centrox)*cos(phi));
    sparabola(:)=val;
    sparabola=sparabola<1;
    para(:,:,ti)=sparabola;
end

% tic;
% parabola = zeros(size(Imbinary));
% for y=1:size(parabola,1)
%     for x=1:size(parabola,2)
%         phi=phi(1);
%         p=p(1);
%         val = ((y-centroy)*cos(phi)-(x-centrox)*sin(phi))^2 - 4*p*((y-centroy)*sin(phi)+(x-centrox)*cos(phi));
%         if(val < 1)
%             parabola(y,x)=1;
%         end
%     end
% end
% toc

% imshow(parabola)
% parabola=im2bw(parabola);
% figure,imshow(Imbinary);
parabola1 = bwmorph(parabola,'erode');
parabola2 = parabola-parabola1;

Imbinary1=double(parabola2);
Imbinary1(:,:,2)=double(parabola2);
Imbinary1(:,:,3)=double(Imbinary);
Imbinary1(Imbinary1(:)>0)=255;
%  overlayedParabola = zeros(size(Imbinary),3);
%  overlayedParabola(:,:,1)=Imbinary(:,:);
%  overlayedParabola(:,:,:)=parabola2;
%  figure,imshow(Imbinary1);
%  hold on;
%  plot([centrox centrox+p*cos(phi)],[centroy centroy+p*sin(phi)],'--rs');
%  hold off;


       % Get the principle axis of parabola
        
        line_image = zeros(size(parabola2));
        [rmax,cmax] =size(line_image);
        i = 1;
        for k = 1:cmax
            x(i) = k;
            y(i) = centroy + (x(i) - centrox)* tan(phi);
            if(y(i) < rmax && y(i)>1)
                line_image(floor(y(i)),floor(x(i))) = 1;
                Imbinary1(floor(y(i)),floor(x(i)))=255;
            end
            %parabola(x(i),floor(y(i))) = 1;
            i = i+1;
        end
        
% figure,imshow(Imbinary1);

end