function [ellval,resnorm1]= fitting_ellipse_aspcheck_v2(outskel,window, varargin)

THR = 0.001;
% pc = 0.4;
if nargin > 2
    pc = varargin{1};
end

dbg = true;
    imsiz = size(outskel); % nr,nc : x=r , y=c, org = topleft
    hsiz = fix(imsiz./2);
    
    init = [hsiz./500, 90*pi/180, 2/3*hsiz(2)/500, 1/1.3];%, 2/3*hsiz(1)/500];

    [Y,X]=meshgrid(1:imsiz(2),1:imsiz(1));

    allpts = [X(:),Y(:)];      

     
%     figure(1),
%     subplot(3,3,7),
%     imshow(outskel,[]), title('target')
    
        
     
    [pts_x, pts_y] = find(outskel);
    lin = find(outskel);
    sampleidx = 1:numel(lin);
    ptratio=min(400, fix(pc*numel(pts_x)));
    sampleidx=randsample(numel(pts_x),ptratio);
    lin = lin(sampleidx);
    sel_tr=pts_x(sampleidx);
    sel_tc=pts_y(sampleidx);
    sel= [sel_tr, sel_tc];
    inpts = [sel_tr, sel_tc]./500; 

    inval = zeros(size(inpts,1),1);
    
%     lb1 = [1/500, 1/500  , (90-10)*pi/180, 120/4/500];
%     ub1 = [hsiz(1)*2/500, hsiz(2)*2/500, (90+10)*pi/180, 640/4/500]; 
    lb1 = [window(1)/500, window(2)/500 , (90-10)*pi/180, 160/4/500, 1/1.8];%, 160/4/500];
    ub1 = [window(3)/500, window(4)/500, (90+10)*pi/180, 700/4/500, 1/1.3];%, 640/4/500]; 
%   lb1 = [-imsiz(1)/2, -imsiz(2)/2, (90-10)*pi/180, -inf, -inf];
%   ub1 = [imsiz(1)*3/2, imsiz(2)*3/2, (90+10)*pi/180, inf, inf];
%   inp=inpts*500;
%   lb1 = [1, 1 , (90-10)*pi/180, 120/4];
%   ub1 = [hsiz(1)*2, hsiz(2)*2, (90+10)*pi/180, 500/4]; 
%   init1=[hsiz, 90*pi/180, 500/4];

    if dbg

        initvf = ellipse(init, allpts./500);
%         lin = find(outskel);
        np = numel(lin);
        tmp2=zeros(imsiz);
        tmp2(:)=initvf<THR;
        skelinitres = initvf(lin);
        tmp2(lin)=1;
% 
        num_inliers0 = sum(skelinitres<THR);
        resnorm0 = sum(skelinitres.^2);
        resnorm0i = sum((skelinitres(skelinitres<THR)).^2);
        resnorm0o = sum((skelinitres(~skelinitres<THR)).^2);

        figure(1)
        subplot(3,4,5),
        imshow(tmp2,[]), 
        title(sprintf('init mse %f/%d : (%f)/%d+ (%f)/%d',...
            resnorm0,np, resnorm0i,num_inliers0,resnorm0o,(np-num_inliers0)) )
        %title('ellipse initializating...');
        
    end

%     ss=statset('nlinfit');
%     ss.Robust='on';
% %     ss.RobustWgtFun='cauchy';
%     [params1, residual1] = nlinfit(inpts,inval,@ellipse, init,ss);
%     residual1 = ellipse(params1, inpts);
%     resnorm1 = sum(residual1.^2);
    
    [params1, resnorm1, residual1, exitflag1] = lsqcurvefit(@ellipse, init, inpts, inval, lb1, ub1);
    rightar=residual1<THR;
    np = numel(residual1);
    num_inliers1=sum(rightar);
    vp= ellipse(params1, allpts./500);
    
    tmp3 = zeros(imsiz);
    tmp3(:)=vp<THR;
    tmp3(lin)=1;
    ellval=[num_inliers1, params1];
    
    resnorm1i = sum((residual1(rightar)).^2);
    resnorm1o = sum((residual1(~rightar)).^2);
    if dbg
    figure(1),
    subplot(3,4,6),imshow(tmp3,[]), 
    title(sprintf('mse: %f/%d (%f)/%d + (%f)/%d',...
        resnorm1,np,resnorm1i,num_inliers1, resnorm1o,(np-num_inliers1)) )
    end
    
%     lb2 = [1/500, 1/500  , (-90-10)*pi/180, 120/4/500];
%     ub2 = [hsiz(1)*2/500, hsiz(2)*2/500, (-90+10)*pi/180, 640/4/500];
% % % % % % %     lb2 = [window(1)/500, window(2)/500 , (-90-10)*pi/180, 120/4/500, 100/4/500];
% % % % % % %     ub2 = [window(3)/500, window(4)/500, (-90+10)*pi/180, 640/4/500, 500/4/500];
% % % % % % % %     inp=inpts*500;
% % % % % % % %     lb2 = [1, 1 , (-90-10)*pi/180, 120/4];
% % % % % % % %     ub2 = [hsiz(1)*2, hsiz(2)*2, (-90+10)*pi/180, 500/4]; 
% % % % % % % %     init1=[hsiz, 90*pi/180, 500/4];
% % % % % % %         
% % % % % % %     [params2, resnorm2, residual2, exitflag2] = lsqcurvefit(@parabola, init, inpts, inval, lb2, ub2);
% % % % % % %     rightar=residual2<0.003;
% % % % % % %     num_inliers2=sum(rightar);
% % % % % % %     vp= ellipse(params2, allpts./500);
% % % % % % %     tmp4 = zeros(imsiz);
% % % % % % %     tmp4(:)=vp<0.003;
% % % % % % %     left_ellval=[num_inliers2, params2];
% % % % % % %     figure(1),
% % % % % % %     subplot(3,2,4),imshow(tmp4,[]), title('fitted-left')
    
% % % % % % %     tmp5=tmp3+tmp4;
% % % % % % %     figure(1),
% % % % % % %     subplot(3,2,5),imshow(tmp5,[]), title('Double-arcade')
% % % % % % %     
% % % % % % %     
%     disp('vaanu')
    
%--- % % % % % % % % % % % % 

function v = ellipse(args,pts)

pts_x = pts(:,1)-args(1);
pts_y = pts(:,2)-args(2);

before = [pts_x, pts_y]';



c = cos(args(3));
s = sin(args(3));


pts_rot = [c, -s; s, c]' * before;
xval=[ones(1,size(pts_rot,2))-(pts_rot(1,:).^2)/args(4)^2]*(args(5)*args(4))^2;

v = (pts_rot(2,:)).^2 - xval;

v=abs(v)';