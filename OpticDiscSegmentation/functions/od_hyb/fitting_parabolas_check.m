function [centpt,corval,misc]=fitting_parabolas_check(outskel,cent,flag,paraimg,window)

    imsiz = size(outskel); % nr,nc : x=r , y=c, org = topleft
    hsiz = fix(imsiz./2);
    [Y,X]=meshgrid(1:imsiz(2),1:imsiz(1));
    allpts = [X(:),Y(:)];   
    init = [90*pi/180, 500/4/500];

    all_pts=[cent;allpts./500];

    initvf = parabola1(init,all_pts);

    tmp2=zeros(imsiz);
    tmp2(:)=initvf (2:end)<0.02; 

    
    lowr=window(1);
    upr=window(2);
    lowc=window(3);
    upc=window(4);
    
    
    vesbox=paraimg(lowr:upr,lowc:upc);
    vesbox=bwmorph(vesbox,'skel','Inf');
    [boxr,boxc]=find(vesbox);
    
   for times=1:numel(boxr)
        [pts_x, pts_y] = find(outskel);

        ptratio=fix(1*numel(pts_x));
        sampleidx=randsample(numel(pts_x),ptratio);
        sel_tr=pts_x(sampleidx);
        sel_tc=pts_y(sampleidx);
        sel= [sel_tr, sel_tc];
        inpts = [sel_tr, sel_tc]./500; 
        center(times,:)=[(boxr(times)+lowr)/500,(boxc(times)+lowc)/500] ;
        
        inp=[center(times,:);inpts];
        inval = zeros(size(inp,1),1);
        if flag==0  
            lb = [(90-30)*pi/180, 60/4/500];
            ub = [(90+30)*pi/180, 640/4/500]; 
        else
            lb = [(-90-30)*pi/180, 60/4/500];
            ub = [(-90+30)*pi/180, 640/4/500];
        end 

        [params, resnorm, residual, exitflag] = lsqcurvefit(@parabola1, init,inp , inval, lb, ub);
        ar=residual<0.003;
        residuals(times)=resnorm;
        num_inliers(times)=sum(ar);
        vp= parabola1(params, all_pts);
        tmp3 = zeros(imsiz);
        tmp3(:)=vp (2:end)<0.003;
        val(times,:)=params;
%         figure(1),
%         subplot(3,3,6),imshow(tmp3+paraimg,[]), title('Corrected Double-arcade')
   end
    [mv,mp]=max(num_inliers);
    corval=val(mp,:);
    centpt=center(mp,:);
    resi=residuals(mp);
    inliers_ratio=mv/sum(outskel(:));
    misc=[inliers_ratio,resi];
    
%     disp('vaanu')
    
%--- % % % % % % % % % % % % 

function v = parabola1(args,pts)

pts_x = pts(:,1)-pts(1,1);
pts_y = pts(:,2)-pts(1,2);

before = [pts_x, pts_y]';

c = cos(-args(1));
s = sin(-args(1));


pts_rot = [c, -s; s, c] * before;

v = (pts_rot(2,:)).^2 - 4*args(2).*(pts_rot(1,:));

v=abs(v)';
