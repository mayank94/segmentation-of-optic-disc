function [v,vs] = parabola(args,pts)

pts_x = pts(:,1)-args(1);
pts_y = pts(:,2)-args(2);

before = [pts_x, pts_y]';



c = cos(args(3));
s = sin(args(3));


pts_rot = [c, -s; s, c]' * before;

v = (pts_rot(2,:)).^2 - 4*args(4).*(pts_rot(1,:));

vs = v';

v=abs(v)';

