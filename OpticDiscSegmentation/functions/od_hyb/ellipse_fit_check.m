dirnames1={'full_OD/diaretdb1_sel',...
'full_OD/dmed_sel','full_OD/messidor_sel',... 
'full_OD/drive_sel','full_OD/sel_out_80','full_OD/indian100'};

dirnamefile={'odmac_prefinal_diaretdb1','odmac_prefinal_dmed',...
    'odmac_prefinal_messidor','odmac_prefinal_drive',...
    'odmac_prefinal_sel_ou','odmac_prefinal_india'};

for di=6%:numel(dirnames1)
    dirname = dirnamefile{di};
    dirname1= dirnames1{di};

    x=dir([dirname '/*.mat']);
    outdir=['ellipse_fit_' dirname(end-5:end)];
    if ~exist(outdir,'dir')
        mkdir(outdir)
    end
    maxim=numel(x);
    ndint=[];
    ndpar=[];
    ndhyb=[];
    ndell=[];
    ndparm=[];
       

    for u=1:maxim
        try
            load(sprintf('%s/%s',dirname,x(u).name));
%             load(sprintf('%s/%s',maccentname,x(u).name));
            
%             load(sprintf('%s/%s',hybridname,x(u).name));
            inp=imread(sprintf('%s/%s',dirname1,x(u).name(1:(end-4))));
        catch
            continue;
        end
        
        origsiz = [size(inp,1),size(inp,2)];
        factor=fix(origsiz(1)/256);
        green=inp(:,:,2);
        
        green1=imresize(green,1/factor);
        [Y,X]=meshgrid(1:size(green1,2),1:size(green1,1));
        pts=[X(:),Y(:)];
        val=parabola(majp,pts./500);
        val1=ellipse(params,pts./500);
        parimg=zeros(size(green1));
        ellimg=zeros(size(green1));
        parimg(:)=val<0.002;
        ellimg(:)=val1<0.002;
        focaldist=sqrt((params(4)*500)^2-(params(5)*500)^2);
        
        xve1=params(1)*500+(params(4)*500*cos(-params(3)));
        yve1=params(2)*500-(params(4)*500*sin(-params(3)));

        xve2=params(1)*500-(params(4)*500*cos(-params(3)));
        yve2=params(2)*500+(params(4)*500*sin(-params(3)));
        
        xfe1=params(1)*500+(focaldist*cos(-params(3)));
        yfe1=params(2)*500-(focaldist*sin(-params(3)));

        xfe2=params(1)*500-(focaldist*cos(-params(3)));
        yfe2=params(2)*500+(focaldist*sin(-params(3)));
        
        ellv1=[yve1,xve1];ellv2=[yve2,xve2];ellf1=[yfe1,xfe1];ellf2=[yfe2,xfe2];
        cent=[params(1)*500,params(2)*500];
        
        parv=[majp(1)*500,majp(2)*500];
        pltpts=[ellv1;ellv2;ellf1;ellf2;fliplr(cent);fliplr(parv)]; 
        distv1=hypot(ellv1(2)-parv(1),ellv1(1)-parv(2));
        distv2=hypot(ellv2(2)-parv(1),ellv2(1)-parv(2));
        distf1=hypot(ellf1(2)-parv(1),ellf1(1)-parv(2));
        distf2=hypot(ellf2(2)-parv(1),ellf2(1)-parv(2));
        distc=hypot(cent(1)-parv(1),cent(2)-parv(2));
        majorlen=params(4)*500;
        minorlen=params(5)*500;
        
%         figure(1),imshow(im2double(parimg)+im2double(ellimg)+im2double(vmap),[]);hold on;
%         plot(pltpts(:,1),pltpts(:,2),'r.','MarkerSize',20);
%         hold off;
%         title(sprintf('v1: %.2f, v2: %.2f, f1: %.2f, f2: %.2f, fd: %.2f, maj: %.2f, min: %.2f, cen: %.2f',...
%             distv1,distv2,distf1,distf2,focaldist,majorlen,minorlen,distc) )
        save(sprintf('%s/distances_%s',outdir,x(u).name),'distv1','distv2','distf1','distf2','distc','focaldist','majorlen','minorlen');
%         print(1,'-dpng',sprintf('%s/ell_det%s',outdir,x(u).name(1:end-4)));
    end
        
        
end