function out=od_entropy(im,fm)
red=im(:,:,1);
red=imadjust(red,[0;1],[0;1],3);
en=entropyfilt(red);
en=en-min(en(:));
en=en./max(en(:));
% ien=1-en;

en=en.*fm;
out=im2bw(en,0.9);
% if sum(out(:))==0
%     ent=en.*fm;
%     out=im2bw(ent,0.9);
% end
% out=imerode(out,strel('disk',5));
out=imdilate(out,strel('disk',5));