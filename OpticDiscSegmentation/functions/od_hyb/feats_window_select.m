function features = feats_window_select(window,ch,vess,imr)

[labels,nlabels]=bwlabel(window);
cc=bwconncomp(window);

red=imr(:,:,1);
green=imr(:,:,2);
blue=imr(:,:,3);

ent=entropyfilt(ch);
ent=ent-min(ent(:));
ent=ent./max(ent(:));

skeleton=bwmorph(vess,'skel','Inf');
outline=bwmorph(vess,'remove');
distmap=bwdist(skeleton);
distmap_out=bwdist(outline);
widthmap=2.*im2double(distmap_out).*im2double(skeleton);

vesselmask=imdilate(vess,strel('square',5));
inpainted=roifill(ch,vesselmask);

canny_vess=edge(ch,'canny',0.4,1.2);
canny_wvess=edge(inpainted,'canny',0.4,1.2);

mean_boundwidth=zeros(nlabels,1);
mean_entropy=zeros(nlabels,1);
mean_vessel=zeros(nlabels,1);
mean_distance=zeros(nlabels,1);
max_vesswidth=zeros(nlabels,1);
min_vesswidth=zeros(nlabels,1);
mean_vesswidth=zeros(nlabels,1);
edge_vess=zeros(nlabels,1);
edge_wvess=zeros(nlabels,1);
mean_red=zeros(nlabels,1);
mean_green=zeros(nlabels,1);
mean_blue=zeros(nlabels,1);
std_red=zeros(nlabels,1);
std_green=zeros(nlabels,1);
std_blue=zeros(nlabels,1);

for i=1:nlabels
    pixlist=cc.PixelIdxList{i};

    th=maxentropie(ch(pixlist));
    
    roi = false(size(green));
    roi(pixlist)=1;
    reg_ch=im2double(ch).*roi;
    region=im2bw(reg_ch,th/255);
    dregion=imdilate(region,strel('square',5));
    

    bound_pixels=cell2mat(bwboundaries(dregion));
    if isempty(bound_pixels)
        width_array=0;
    else
        for ip=1:size(bound_pixels,1)
            width_array(ip)=widthmap(bound_pixels(ip,1),bound_pixels(ip,2));       
        end
    end
    mean_boundwidth(i)=mean(width_array);
    mean_entropy(i)=mean(ent(pixlist));
    mean_vessel(i)=mean(vess(pixlist));
    mean_distance(i)=mean(distmap(pixlist));
    max_vesswidth(i)=max(widthmap(pixlist));
    min_vesswidth(i)=min(widthmap(pixlist));
    mean_vesswidth(i)=mean(widthmap(pixlist));
    edge_vess(i)=mean(canny_vess(pixlist));
    edge_wvess(i)=mean(canny_wvess(pixlist));
    mean_red(i)=mean(im2double(red(pixlist)));
    mean_green(i)=mean(im2double(green(pixlist)));
    mean_blue(i)=mean(im2double(blue(pixlist)));
    std_red(i)=std(im2double(red(pixlist)));
    std_green(i)=std(im2double(green(pixlist)));
    std_blue(i)=std(im2double(blue(pixlist)));

end
features=[mean_boundwidth,mean_entropy,mean_vessel,mean_distance,...
        max_vesswidth,min_vesswidth,mean_vesswidth,edge_vess,...
        edge_wvess,mean_red,mean_green,mean_blue,std_red,std_green,...
        std_blue];


