function [linimage,params]=axis_determine(vess)
skeleton=bwmorph(vess,'skel','Inf');
window=[1,1,size(skeleton,1),size(skeleton,2)];
[ellval]= fitting_ellipse_check(skeleton,window);
params=ellval(2:end);
linimage=zeros(size(vess));

[X,Y]=meshgrid(1:size(vess,2),1:size(vess,1));

allpts = [X(:),Y(:)];  
value=allpts(:,2)-((tan(-(params(3)-(pi/2)))*(allpts(:,1)-params(2)*500))+params(1)*500);
linimage(:)=value;

if true
    figure(1),
    subplot(3,3,3),imshow(vess>0 | abs(linimage)<3)
end

linimage=linimage>0;
