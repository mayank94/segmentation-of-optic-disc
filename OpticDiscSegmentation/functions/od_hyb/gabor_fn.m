function img_out = gabor_fn(img, scale, theta)
% bw    = bandwidth, (1)
% gamma = aspect ratio, (0.5)
% psi   = phase shift, (0)
% lambda= wave length, (>=2)
% theta = angle in rad, [0 pi)

lambda  = scale;
gamma   = 0.5;
bw      = 1;


% img_out = zeros(size(img));

% sigma = lambda/pi*sqrt(log(2)/2)*(2^bw+1)/(2^bw-1);
sigma = scale./(2*sqrt(2*log(6.)));


sigma_x = sigma;
sigma_y = sigma/gamma;

sz=fix(8*max(sigma_y,sigma_x));
if mod(sz,2)==0, sz=sz+1;end

% alternatively, use a fixed size
% sz = 60;
 
[x y]=meshgrid(-fix(sz/2):fix(sz/2),fix(sz/2):-1:fix(-sz/2));
% x (right +)
% y (up +)

% Rotation 
x_theta=x*cos(theta*pi/180)+y*sin(theta*pi/180);
y_theta=-x*sin(theta*pi/180)+y*cos(theta*pi/180);
 
gb=exp(-0.5*(x_theta.^2/sigma_x^2 + y_theta.^2/sigma_y^2)).*cos(2*pi/lambda*x_theta) ;
%imtool(gb/2+0.5);

% img_out = imfilter(img, gb, 'symmetric');
% img_out = img_out./max(img_out(:));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%garima
% fft_fliter_sze = 2^nextpow2(max(size(img)));
% fft_inputImage1 = fft2(img, fft_fliter_sze, fft_fliter_sze);
% 
% fun_padded = zeros(fft_fliter_sze);
% sze = fix(sz/2);
% hs = fix(fft_fliter_sze/2);
% fun_padded(hs-sze:hs+sze,hs-sze:hs+sze) = gb;
% fft_fun = fft2(fun_padded,fft_fliter_sze,fft_fliter_sze); 
% 
% gaborresp = ifftshift(ifft2((fft_inputImage1).*fft_fun));
% img_out = abs(gaborresp(1:size(img,1),1:size(img,2)));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%garima


A = img; % image
B = gb; % some 2D filter function

[m,n] = size(A);
[mb,nb] = size(B); 
% output size 
mm = m + mb - 1;
nn = n + nb - 1;

A = padarray(A, [ceil((mm-m)/2), ceil((nn-n)/2)], 'replicate', 'pre');
A = padarray(A, [floor((mm-m)/2), floor((nn-n)/2)], 'replicate', 'post');

% A = padarray(A, [mm-m, nn-n], 'replicate', 'pre');

% pad, multiply and transform back
C = ifft2(fft2(A).* fft2(B, mm, nn));

% padding constants (for output of size == size(A))
% padC_m = ceil((mb-1)./2);
% padC_n = ceil((nb-1)./2);

% frequency-domain convolution result
img_out = C(mb:end, nb:end);