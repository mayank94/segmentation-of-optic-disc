function [mask,boxsize] = OD_boundary(ch,out,fm)
[label,nl]=bwlabel(out);
props=regionprops(label,'BoundingBox');
props=struct2cell(props);
props=cell2mat(props');
mask=zeros(size(ch));
for i=1:nl
% i
rowrange_low = fix(props(i,2));
rowrange_high= fix(props(i,2)+props(i,4));
colrange_low = fix(props(i,1));
colrange_high= fix(props(i,1)+props(i,3));
if rowrange_low<1
    rowrange_low=1;
end
if rowrange_high>size(ch,1)
    rowrange_low=size(ch,1);
end
if colrange_low<1
    colrange_low=1;
end
if colrange_high>size(ch,2)
    colrange_high=1;
end
    ch_box=ch(rowrange_low:rowrange_high,colrange_low:colrange_high);
    
    [~,mp]=max(ch_box(:));
    [mpr,mpc]=ind2sub(size(ch_box),mp);
    maxrpos=props(i,2)+mpr;
    maxcpos=props(i,1)+mpc;    
    boxsize(i,:)=make_box(maxrpos,maxcpos,fix(size(ch,1)/3),out);
    lowr=boxsize(i,1); upr=boxsize(i,2); lowc=boxsize(i,3); upc=boxsize(i,4);

    bb=ch(lowr:upr,lowc:upc);
    fb=fm(lowr:upr,lowc:upc);
    med=medfilt2(bb,[15 15]);
%     bbsub=imcomplement(bb)-imcomplement(med);
% 
%     [h1,cen] = hist(bbsub(:),[0:2:255]);
%     h2=cumsum(fliplr(h1))/sum(h1);
%     x1 = find(h2 > 0.1, 1, 'first');
%     x1 = cen(end-x1)+1;
%     Ves= bbsub> x1;
%     
%     vessel=imdilate(Ves,strel('square',5));
%     inpainted=roifill(bb,vessel);
%     processed=imclose(inpainted,strel('disk',10));
%     edgemap=edge(processed,'canny',0.4,1.2);
%     fmer=imerode(fb,strel('disk',10));
%     edgemap=fmer&edgemap;
%     edgemap2=0*edgemap;
%     [r,c]=find(edgemap);
%     k=convhull(r,c);
%     for t=k'
%         edgemap2(r(t),c(t))=1;
%     end
% %     od=poly2mask(c(k),r(k),size(bb,1),size(bb,2));
%     mask(lowr:upr,lowc:upc)=edgemap2;
end