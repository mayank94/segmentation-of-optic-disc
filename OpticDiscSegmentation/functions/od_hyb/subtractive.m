function  Ipp2=subtractive(im,fm)

% figure,imshow(Ig,[]),title('image'),pixval on, 
% pause

[nr nc d]=size(im);

Ig=[];
if d==1
  Ig=im(1:end,1:end);
else
  Ig=im(1:end,1:end,2);
end

[nr nc]=size(Ig);

ms=[24,32];

if nr > 500
  ms = [44,44];
  if nr > 1000
      ms=[48,60];
      if nr > 1200
          ms=[52,52];
      end
  end
end

% ms

Ibg=medfilt2(Ig,ms,'symmetric');
Ibg = Ibg.*uint8(fm);
% figure,imshow(uint8(Ibg)),title('background'),pixval on
% pause

%Isc = Ibg-Ig;

% Isc = double(Ibg) - double(Ig) ;

Isc = double(Ig) - double(Ibg);

me=mean(Isc(fm));

% imshow(Isc,[]),title('shade corrected'),pixval on,pause;

Ipp = Isc .* (Isc<=me);

mM=minmax(Ipp(:)');

Ipp2=uint8(255*(Ipp-mM(1))/(mM(2)-mM(1)));

return

% x=min(Ipp(:))
% Ipp2=Ipp-x; %make min=0
% 
% x=2*max(Ipp2(:))
% Ipp2=Ipp2./x; %make max=1

%Ipp2=Ipp2.*(Ipp2<1);

%imshow(Ipp2,[]),colormap('jet'),title('no bright lesions')
%pixval on;

