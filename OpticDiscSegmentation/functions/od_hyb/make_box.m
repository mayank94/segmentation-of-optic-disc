function box_coord = make_box(rval,cval,span,img)
lowr=rval-fix(span/2);
lowc=cval-fix(span/2);
upr=rval+fix(span/2);
upc=cval+fix(span/2);
if lowr<1
    lowr=1;
end
if lowc<1
    lowc=1;
end
if upr>size(img,1)
    upr=size(img,1);
end
if upc>size(img,2)
    upc=size(img,2);
end
box_coord=[lowr,upr,lowc,upc];
return;