function [ final_image,im_area,imbin ] = optic_disc_initial( I,th )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

load lin_class2;

%Preprocess
I = rgbnormalize1(I);
I2 = I;
% grhist = imhist(I2(:,:,2));
% af = histeq(I2(:,:,1),grhist);
% I2(:,:,1) = af;
I2 = imfilter(I2,fspecial('gaussian',5,2));
str = strel('disk',70);
im_close = imclose(I2(:,:,1),str);
im_open = imopen(im_close,str);
im_reconstruct = imreconstruct(im_open,I2(:,:,1));
str = strel('disk',10);
grad = imdilate(im_reconstruct,str)-imerode(im_reconstruct,str);
im_wth = imtophat(grad,str);

%Watershed
im_wsd = watershed(im_wth);
% for m = 1:max(max(im_wsd))
% intensity(m) = regionprops(im_wsd==m,I(:,:,1),'MeanIntensity','MajorAxisLength');
% value(m) = intensity(m).MeanIntensity;
% end
% [A B] = max(value);
% normalise = intensity(B).MajorAxisLength;
%X = (im_wsd==B);
% 
% %Prepare test image
% im_test = im_test*255;
% im_test = im2uint8(im_test);
% 
% % %Find number of labels
% l=union(im_wsd(im_test>0),[]);
% 
% %Remove small sectors
% b=1;m=[];
% for i =1:size(l,1)
%     total = sum(sum(im_wsd==l(i)));
%     X = (im_wsd==l(i));
%     X = im2uint8(X*255);
%     overlap = (X.*im_test)/255;
%     if sum(sum(overlap))<(0.1*total)
%         m(b) = l(i);
%         b=b+1;
%     end
% end
% l = setxor(l,m);
%   
% %Set non-mask elements to zero
% im_wsd2 = im_wsd;
% for i =0: max(max(im_wsd))
%     if any(ismember(i,l))==0
%         im_wsd2(im_wsd2==i)= 0;
%     end
% end

%Extract features
img_avg = (I(:,:,1)+I(:,:,2)+I(:,:,3))/3;
for i = 1: max(max(im_wsd))
    j = (im_wsd==i);
    red = regionprops(j,I(:,:,1),'MeanIntensity');
    green = regionprops(j,I(:,:,2),'MeanIntensity');
    blue = regionprops(j,I(:,:,3),'MeanIntensity');
    centroid = regionprops(j,img_avg,'WeightedCentroid');
    image_centre = [round(size(I,1)) round(size(I,2))];
    centroidloc = centroid.WeightedCentroid;
    mean_red(i) = red.MeanIntensity;
    mean_green(i) = green.MeanIntensity;
    mean_blue(i) = blue.MeanIntensity;
    mean_distance(i) = sqrt((centroidloc(1)-image_centre(1))^2+(centroidloc(2)-image_centre(2))^2)/size(I,1);
    std_dev_sum(i) = mean_red(i)/mean_green(i);
end
    features = vertcat(mean_red,mean_green,mean_blue,mean_distance,std_dev_sum);
    features = features';
    

%Create initial fit
im_wsd2 = im_wsd;
img_wsd = double(im_wsd);
[check,probab] = predict(clas,features(:,1:4));
probab(:,2) = probab(:,2)/max(probab(:,2));
im_wsd2 = double(im_wsd2);
%     histog = hist(im_wsd2(:),64);
%     total = cumsum(histog);
%     total = total/numel(im_wsd2);
%     th = find(total>0.65,1,'first');
%     th = th*4/255;
%th = min(probab);
for i =1:size(check)
    if probab(i,2)>th
        im_wsd2(im_wsd==i)= 1;
        img_wsd(im_wsd==i) = probab(i,2);
    else
        im_wsd2(im_wsd==i)=0;
        img_wsd(im_wsd==i) = 0;
    end
end

%Postprocess
str = strel('disk',1);
im_area = imdilate(img_wsd,str);
% im_rebuild = imerode(im_area,str);
im_rebuild = uint8(im_area);
im_label = bwlabel(im_rebuild);
cent = im_label(round(size(im_label,1)/2),round(size(im_label,2)/2));
im_cent = (im_label==cent);
[x,y] = find(bwperim(im_cent));
im_rel = im_area>0;

%Take relevant points
prop = regionprops(im_rel,im_area,'WeightedCentroid');
centroid1 = prop.WeightedCentroid;
for i=1:size(x)
    distance(i) = sqrt((x(i)-centroid1(1))^2+(y(i)-centroid1(2))^2);
end
mean_dist = mean(distance);
std_dev = std(distance); 
a=1;
for i = 1:size(distance,2)
    if distance(i)<(mean_dist+std_dev)
        x(a) = x(i);y(a) = y(i);
        a=a+1;
    end
end

%Fit the output
[jab,jab2,jab3] = fit_ellipse(x,y,gca);
imbin = poly2mask(jab3(2,:),jab3(1,:),size(I(:,:,1),1),size(I(:,:,1),2));
imbin = 255*uint8(imbin);

%Produce final result
final_image = (I(:,:,2)+uint8(255*bwperim(imbin)));
%output = evaluate(im_test,imbin);

end

