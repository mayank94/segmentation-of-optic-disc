 %Load images
%observations = [];
%train_labels = [];
outputs = [];

directory = dir('rim-one-r1\Normal');
for k = 9:7:37
    fname = directory(k).name;
    testname = directory(k-6).name;
    I = imread(strcat('rim-one-r1\Normal\',fname));
    im_test = imread(strcat('rim-one-r1\Normal\',testname));

%Contrast enhance
% x=double(max(max(I(:,:,1))));
%  histog = imhist(I(:,:,1));
%     histogram = flipud (histog);
%     total_val = cumsum(histogram)/numel(I(:,:,1));
%     tint = find(total_val>0.1,1,'first');
%     th= (255-tint);
%     gamma = log(255)/log(th);
% img = (double(I(:,:,1)).^gamma)/(x^gamma)*255;

%Preprocess
str = strel('disk',70);
im_close = imclose(I(:,:,1),str);
im_open = imopen(im_close,str);
im_reconstruct = imreconstruct(im_open,I(:,:,1));
str = strel('disk',10);
grad = imdilate(im_reconstruct,str)-imerode(im_reconstruct,str);
im_wth = imtophat(grad,str);

%Watershed
im_wsd = watershed(im_wth);
for m = 1:max(max(im_wsd))
intensity(m) = regionprops(im_wsd==m,I(:,:,1),'MeanIntensity','MajorAxisLength');
value(m) = intensity(m).MeanIntensity;
end
[A B] = max(value);
normalise = intensity(B).MajorAxisLength;
%X = (im_wsd==B);

%Prepare test image
im_test = im_test*255;
im_test = im2uint8(im_test);

% %Find number of labels
l=union(im_wsd(im_test>0),[]);

%Remove small sectors
b=1;m=[];
for i =1:size(l,1)
    total = sum(sum(im_wsd==l(i)));
    X = (im_wsd==l(i));
    X = im2uint8(X*255);
    overlap = (X.*im_test)/255;
    if sum(sum(overlap))<(0.1*total)
        m(b) = l(i);
        b=b+1;
    end
end
l = setxor(l,m);
  
%Set non-mask elements to zero
im_wsd2 = im_wsd;
for i =0: max(max(im_wsd))
    if any(ismember(i,l))==0
        im_wsd2(im_wsd2==i)= 0;
    end
end
%labels = ismember(0:i,l);

%sf=stdfilt(I(:,:,2),ones(9));
%Extract features
img_avg = (I(:,:,1)+I(:,:,2)+I(:,:,3))/3;
for i = 0: max(max(im_wsd))
    j = (im_wsd==i);
    red = regionprops(j,I(:,:,1),'MeanIntensity');
    green = regionprops(j,I(:,:,2),'MeanIntensity');
    blue = regionprops(j,I(:,:,3),'MeanIntensity');
    centroid = regionprops(j,img_avg,'WeightedCentroid');
    image_centre = [round(size(I,1)) round(size(I,2))];
    centroidloc = centroid.WeightedCentroid;
    mean_red(i+1) = red.MeanIntensity;
    mean_green(i+1) = green.MeanIntensity;
    mean_blue(i+1) = blue.MeanIntensity;
    mean_distance(i+1) = sqrt((centroidloc(1)-image_centre(1))^2+(centroidloc(2)-image_centre(2))^2)/normalise;
    %std_dev(i+1) = 10* mean(sf(j));
    std_dev_sum(i+1) = sum(sum(stdfilt(j)));
end
features = vertcat(mean_red,mean_green,mean_blue,mean_distance,std_dev_sum);
features = features';
%observations = vertcat(observations,features);
%train_labels = horzcat(train_labels,labels);
%clearvars -except k observations train_labels directory clas

%Check reconstructed disk
%clas = fitensemble(observations,train_labels,'AdaBoostM1',500,'tree');
[check,probab] = predict(clas,features);
% correct = 0;
% for i=1:size(check,1)
%     if check(i)==train_labels(i)
%         correct = correct+1;
%     end
% end
% accuracy = 100*correct/size(check,1);
th = min(probab);
for i =1:size(check)
    if probab(i,2)<0.75*th(2)
        im_wsd2(im_wsd2==i)= 0;
    else
        im_wsd2(im_wsd2==i)=65535;
    end
end

%Postprocess
str = strel('disk',15);
im_area = imdilate(im_wsd2,str);
im_rebuild = imerode(im_area,str);
im_rebuild = uint8(im_rebuild);
[x,y] = find(bwperim(im_rebuild));

%Take relevant points
prop = regionprops(im_rebuild==255,'Centroid');
centroid1 = prop.Centroid;
for i=1:size(x)
    distance(i) = sqrt((x(i)-centroid1(1))^2+(y(i)-centroid1(2))^2);
end
mean_dist = mean(distance);
std_dev = std(distance); 
a=1; p = [];
for i = 1:size(distance,2)
    if distance(i)<(mean_dist+std_dev)
        x(a) = x(i);y(a) = y(i);
        a=a+1;
    end
end

%Fit the output
[jab,jab2,jab3] = fit_ellipse(x,y,gca);
imbin = poly2mask(jab3(2,:),jab3(1,:),size(I(:,:,1),1),size(I(:,:,1),2));
imbin = 255*uint8(imbin);

%Produce final result
final_image = (I(:,:,2)+uint8(255*bwperim(imbin)));
outputs =vertcat(outputs,evaluate(im_test,imbin));
imwrite(final_image,fname,'bmp');
clearvars -except k directory clas outputs

end

xlswrite('Test characters.xls',outputs);

% %Check amount of overlap
% percent=0;overlap=0;
% for i = 1:size(I,1)
%     for j = 1:size(I,2)
%         if ((im_rebuild(i,j)==im_test(i,j)))
%             overlap = overlap+1;
%         end
% %         if (im_test(i,j)==255)
% %             percent = percent+1;
% %         end
%     end
% end
% overlap_percentage = overlap/(size(I,1)*size(I,2))*100
% imshow(im_rebuild)
% figure,imshow(I)