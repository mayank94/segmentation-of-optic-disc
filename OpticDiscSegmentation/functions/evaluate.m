function [output] = evaluate(im_test,im_rebuild)

%Dynes coeffecient
im_test = uint8(255*im_test);
union_matrix = im_test+im_rebuild;
intersection_matrix(size(im_rebuild,1),size(im_rebuild,2)) = 0;
for i=1:size(im_rebuild,1)
    for j=1:size(im_rebuild,2)
        if im_rebuild(i,j)==255 && im_test(i,j)==255
            intersection_matrix(i,j) = 255;
        end
    end
end
dice_coeffecient = sum(sum(intersection_matrix))/sum(sum(union_matrix));
output(1,1) = dice_coeffecient;

%Distance transform and sign change
[dist,map] = bwdist(bwperim(im_rebuild));
pp=find(bwperim(im_test));
v = dist(pp);
nd = map(pp);
sel=im_test(nd)>0;
v(sel)=-1*v(sel);

%Find values
a=1;b=1;vpos=[];vneg=[];
for i=1:size(v,1)
    if v(i)>0
        vpos(a) = v(i);
        a = a+1;
    elseif v(i)<0
        vneg(b) = v(i);
        b=b+1;
    end
end   
if isempty(vneg)
    vneg=0;
end
if isempty(vpos)
    vpos=0;
end
output(1,2) = max(vpos);output(1,3) = min(vpos);
output(1,4) = max(vneg);output(1,5) = min(vneg);

end