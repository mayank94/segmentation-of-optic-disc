for i = 1:50
img_wsd = soln{i};

%Postprocess
str = strel('disk',1);
im_area = imdilate(img_wsd,str);
% im_rebuild = imerode(im_area,str);
im_rebuild = uint8(im_area);
im_label = bwlabel(im_rebuild);
cent = im_label(round(size(im_label,1)/2),round(size(im_label,2)/2));
im_cent = (im_label==cent);
[x,y] = find(bwperim(im_cent));
im_rel = im_area>0;

%Take relevant points
prop = regionprops(im_rel,im_area,'WeightedCentroid');
centroid1 = prop.WeightedCentroid;
for j=1:size(x)
    distance(j) = sqrt((x(j)-centroid1(1))^2+(y(j)-centroid1(2))^2);
end
mean_dist = mean(distance);
std_dev = std(distance); 
a=1;
for j = 1:size(distance,2)
    if distance(j)<(mean_dist+std_dev)
        x(a) = x(j);y(a) = y(j);
        a=a+1;
    end
end

%Fit the output
[jab,jab2,jab3] = fit_ellipse(x,y,gca);
imbin = poly2mask(jab3(2,:),jab3(1,:),size(img_wsd,1),size(img_wsd,2));
imbin_array{i} = 255*uint8(imbin);

clearvars -except i imbin_array soln
end