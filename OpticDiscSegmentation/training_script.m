%Load images
% observations = [];
% train_labels = [];
% im_no = [];
% im_array = [];

directory = dir('rim-one-r1\Moderate');
for k = 9:7:37
    fname = directory(k).name;
    testname = directory(k-6).name;
    I = imread(strcat('rim-one-r1\Moderate\',fname));
    im_test = imread(strcat('rim-one-r1\Moderate\',testname));

%Preprocess
I = rgbnormalize1(I);
I = imfilter(I,fspecial('gaussian',5,2));
str = strel('disk',70);
im_close = imclose(I(:,:,1),str);
im_open = imopen(im_close,str);
im_reconstruct = imreconstruct(im_open,I(:,:,1));
str = strel('disk',10);
grad = imdilate(im_reconstruct,str)-imerode(im_reconstruct,str);
im_wth = imtophat(grad,str);

%Watershed
im_wsd = watershed(im_wth);
im_array{(k-2+315)/7} = im_wsd;
% for m = 1:max(max(im_wsd))
% intensity(m) = regionprops(im_wsd==m,I(:,:,1),'MeanIntensity','MajorAxisLength');
% value(m) = intensity(m).MeanIntensity;
% end
% [A B] = max(value);
% normalise = intensity(B).MajorAxisLength;
%X = (im_wsd==B);

%Prepare test image
im_test = im_test*255;
im_test = im2uint8(im_test);

%Find number of labels
l=union(im_wsd(im_test>0),[]);

%Remove small sectors
b=1;m=[];
for i =1:size(l,1)
    total = sum(sum(im_wsd==l(i)));
    X = (im_wsd==l(i));
    X = im2uint8(X*255);
    overlap = (X.*im_test)/255;
    if sum(sum(overlap))<(0.05*total)
        m(b) = l(i);
        b=b+1;
    end
end
l = setxor(l,m);

%Set non-mask elements to zero
im_wsd2 = im_wsd;
labels=zeros(1,max(max(im_wsd)));
for i =1: max(max(im_wsd))
    if any(ismember(i,l))==0
        im_wsd2(im_wsd==i)= 0;
    else
        im_wsd2(im_wsd==i)= 65535;
        labels(i) = 1;
    end
end


%sf=stdfilt(I(:,:,2),ones(9));
%Extract features
img_avg = (I(:,:,1)+I(:,:,2)+I(:,:,3))/3;
for i = 1: max(max(im_wsd))
    j = (im_wsd==i);
    red = regionprops(j,I(:,:,1),'MeanIntensity');
    green = regionprops(j,I(:,:,2),'MeanIntensity');
    blue = regionprops(j,I(:,:,3),'MeanIntensity');
    centroid = regionprops(j,img_avg,'WeightedCentroid');
    image_centre = [round(size(I,1)) round(size(I,2))];
    centroidloc = centroid.WeightedCentroid;
    mean_red(i) = red.MeanIntensity;
    mean_green(i) = green.MeanIntensity;
    mean_blue(i) = blue.MeanIntensity;
    mean_distance(i) = sqrt((centroidloc(1)-image_centre(1))^2+(centroidloc(2)-image_centre(2))^2)/size(I,1);
    %std_dev(i+1) = 10* mean(sf(j));
    %std_dev_sum(i) = sum(sum(stdfilt(j)));
end
features = vertcat(mean_red,mean_green,mean_blue,mean_distance);
features = features';
observations = vertcat(observations,features);
train_labels = horzcat(train_labels,labels);
im_no = vertcat(im_no,(k+315)*ones(numel(labels),1));
clearvars -except k observations train_labels directory clas im_no im_array
end

%Leave-one-out-test
outputs = [];
for k =9:7:352
    locs = find(im_no==k);
    train_features = observations;
    train_labels2 = train_labels;
    test_features = train_features(locs,:);
    train_features(locs,:) = [];
    train_labels2(locs) = [];
    clas = LinearModel.fit(train_features,train_labels2);
    [check,probab] = predict(clas,test_features);
    probab(:,2) = probab(:,2)/max(probab(:,2));
    img_wsd = im_array{(k-2)/7};
     img_wsd = double(img_wsd);
    for i = 1:size(check)
        if probab(i,2)>0.4519
            img_wsd(img_wsd==i)=probab(i,2);
        else
            img_wsd(img_wsd==i)= 0 ;
        end
    end
    outputs = vertcat(outputs,probab(:,2));
%     histog = hist(img_wsd(:),64);
%     total = cumsum(histog);
%     total = total/numel(img_wsd);
%     th = find(total>0.55,1,'first');
%     th = th*4/255;
%     for i = 1:size(check)
%             if probab(i,2)>th
%                 img_wsd(img_wsd==i)=probab(i,2);
%             else
%                 img_wsd(img_wsd==i)= 0 ;
%             end
%     end
    soln{(k-2)/7} = img_wsd;
end

[X,Y,T,AUC] = perfcurve(train_labels,outputs,'1');
plot(X,Y);
AUC
    
    
% [check,probab] = predict(clas,observations);
% correct = 0;
% for i=1:size(check,1)
%     if check(i)==train_labels(i)
%         correct = correct+1;
%     end
% end
% accuracy = 100*correct/size(check,1);
% th = min(probab);
% for i =1:size(check)
%     if probab(i,2)<0.75*th(2)
%         im_wsd2(im_wsd2==i)= 0;
%     else
%         im_wsd2(im_wsd2==i)=65535;
%     end
% end
% 
% %Postprocess
% str = strel('disk',15);
% im_area = imdilate(im_wsd2,str);
% im_rebuild = imerode(im_area,str);
% 
% imshow(im_rebuild,[]);

% %Check amount of overlap
% percent=0;overlap=0;
% for i = 1:size(I,1)
%     for j = 1:size(I,2)
%         if ((im_rebuild(i,j)==im_test(i,j)))
%             overlap = overlap+1;
%         end
% %         if (im_test(i,j)==255)
% %             percent = percent+1;
% %         end
%     end
% end
% overlap_percentage = overlap/(size(I,1)*size(I,2))*100
% imshow(im_rebuild)
% figure,imshow(I)