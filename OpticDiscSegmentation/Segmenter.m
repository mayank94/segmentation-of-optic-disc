function varargout = Segmenter(varargin)
% SEGMENTER MATLAB code for Segmenter.fig
%      SEGMENTER, by itself, creates a new SEGMENTER or raises the existing
%      singleton*.
%
%      H = SEGMENTER returns the handle to a new SEGMENTER or the handle to
%      the existing singleton*.
%
%      SEGMENTER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in SEGMENTER.M with the given input arguments.
%
%      SEGMENTER('Property','Value',...) creates a new SEGMENTER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Segmenter_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Segmenter_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Segmenter

% Last Modified by GUIDE v2.5 05-Aug-2015 12:38:58

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Segmenter_OpeningFcn, ...
                   'gui_OutputFcn',  @Segmenter_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before Segmenter is made visible.
function Segmenter_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Segmenter (see VARARGIN)
addpath(genpath('functions\'));
% Choose default command line output for Segmenter
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% This sets up the initial plot - only do when we are invisible
% so window can get raised using Segmenter.
% if strcmp(get(hObject,'Visible'),'off')
%     plot(rand(5));
% end

% UIWAIT makes Segmenter wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Segmenter_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file2,loc] = uigetfile('*.bmp');
if ~isequal(file2, 0)
    groundtruth = imread([loc '/' file2]);
end
output = evaluate(groundtruth,handles.rebuild);
uitable(Segmenter,'Data',output,'ColumnWidth',{125},'Position',[300 50 925 50],'ColumnName',{'Dice','Max+','Min+','Min-','Max-'});
final_img = cat(3,handles.image,handles.image,handles.image);
G = bwperim(groundtruth);
axes(handles.axes2);
cla;
final_img(:,:,3)=final_img(:,:,3)+uint8(255*G);
imshow(final_img,[])


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file,loc] = uigetfile('*.bmp');
if ~isequal(file, 0)
    currentim = imread([loc '/' file]);
end
axes(handles.axes1);
cla;
imshow(currentim);
handles.currentim = currentim;
guidata(hObject,handles);



% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
     set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', {'plot(rand(5))', 'plot(sin(1:0.01:25))', 'bar(1:.5:10)', 'plot(membrane)', 'surf(peaks)'});


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes2);
cla;
I = handles.currentim;
th = handles.th;
[final_image,img_wsd,imbin] = optic_disc_initial(I,th);
handles.rebuild = imbin;
%uitable(Segmenter,'Data',output,'ColumnWidth',{125},'Position',[300 50 925 50],'ColumnName',{'Dice','Max+','Min+','Min-','Max-'});
imshow(final_image);
handles.image = final_image;
axes(handles.axes1);
cla;
imshow(img_wsd,[]);impixelinfo
guidata(hObject,handles);



% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes2);
cla;
I = handles.currentim;
%im_test = handles.groundtruth;
[~,~,~,finres] = opticdisc_detection_v3(I,im2bw(I(:,:,2),150/255));
imr = cropper(I,finres);
%im_test = cropper(im_test,finres);
% [final_image,output] = optic_disc_initial(imr,im_test);
% uitable(Segmenter,'Data',output,'ColumnWidth',{125},'Position',[300 50 925 50],'ColumnName',{'Dice','Max+','Min+','Min-','Max-'});
% imshow(final_image);
handles.currentim = imr;
axes(handles.axes2);
cla;
imshow(imr);
guidata(hObject,handles);


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
val = get(hObject,'Value');
handles.th = val;
guidata(hObject,handles);
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
set(hObject, ...
    'Min', 0.0, ...
    'Max', 1.0, ...
    'SliderStep', [0.01 0.1], ...
    'Value', 0.5);
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[1 1 1]);
end
handles.th = 0.5;
guidata(hObject,handles);
