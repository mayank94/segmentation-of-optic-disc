function vmap=symmetric_vmap(rec_im,acsis,fmask1)

iacsis=acsis<1;
acsis=acsis&fmask1;
iacsis=iacsis&fmask1;
[hist1,cen] = hist(double(rec_im(acsis)),[0:2:255]);

hist2=cumsum(fliplr(hist1))/sum(hist1);
x1 = find(hist2 > 0.05, 1, 'first');
x1 = cen(end-x1)+1;
Ves= (double(rec_im).*acsis) > x1;
Ves=Ves.*fmask1;
vess1 = Ves;

[hist1,cen] = hist(double(rec_im(iacsis)),[0:2:255]);

hist2=cumsum(fliplr(hist1))/sum(hist1);
x2 = find(hist2 > 0.05, 1, 'first');
x2 = cen(end-x2)+1;
Ves= (double(rec_im).*iacsis) > x2;
Ves=Ves.*fmask1;
vess2 = Ves;

vmap=vess1+vess2;



