function [ell_out,bestparams,misc]=ellipse_fitting_module_v2(ves_skel,majp)
    
%     window=ws;
    
	% figure,imshow(bwlabel(skel_seg),[])
    window=[1,1,size(ves_skel,1),size(ves_skel,2)];
    tmp=imfilter(uint8(ves_skel),ones(3));
	% figure,imshow(tmp<3);
	skel_seg = ves_skel & (tmp<4);

	cc1 = bwconncomp(skel_seg);
    reg=regionprops(cc1,'Area');
    reg=[reg.Area];
    arar=reg>15;
    cc.NumObjects=sum(arar);
    cc.PixelIdxList=cc1.PixelIdxList(arar);
    
    num_iter=2;
    trial=1;
    np = cc.NumObjects; %sum(skeleton(:));
    numinliers=[];
    Params = [];
    seg_sel = [];
    
    while num_iter>trial
       pc=0.70; 
       seg_sel(trial,:) = randsample(1:np, fix(pc*np));
       skel_inp = false(size(ves_skel));
       
       for si=seg_sel(trial,:)
           pix = cc.PixelIdxList{si};
           skel_inp(pix)=1;
       end
       npi=sum(skel_inp(:));
       [ellval,resnorm]= fitting_ellipse_check_v2(skel_inp,window,pc,majp); 
       numinliers(trial)=ellval(1)/(npi);
       Params(trial,:)=ellval(2:end);
	   residuals(trial)=resnorm;
       pnout=1-(numinliers(trial)^4); %(pc*np));
       pnout=max(eps,pnout);
       pnout=min(1-eps,pnout);
       num_iter=log(0.01)/log(pnout);
       trial=trial+1;
       if trial>1.2*np*pc
           break
       end
    end
    [mv,mi]=max(numinliers);
    bestparams = Params(mi,:);
	

        max_inliers=mv;
        % cent(1)=evm(pe,2);
        % cent(2)=evm(pe,3);
        [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
        allpts = [X(:),Y(:)];
        % params=[cent(1), cent(2), evm(pe,4), evm(pe,5), evm(pe,6)];
        points1=ellipse(bestparams,allpts./500);
        outfig1=zeros(size(ves_skel));
        outfig1(:)=points1;
        finout1=outfig1<0.002;
        inliers_ratio=max_inliers;
        residual_val=residuals(mi);
        misc=[inliers_ratio,residual_val];

    ell_out=finout1;
%     figure(1)
%         subplot(3,4,10),
%         imshow(ell_out,[]), 
%         title('final ellipse');

    return    