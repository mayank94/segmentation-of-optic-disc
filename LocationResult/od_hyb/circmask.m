% Creating a circular mask.
function [cm,cidx]=circmask(C,rad,Imgsize)

% [C,R]=meshgrid(C(2)-[1:Imgsize(2)],C(1)-[1:Imgsize(1)]);
% d = hypot(R,C);
% cm = d<=rad;
% cidx = find(cm);

cm=false(Imgsize);
cen_r=C(1);
cen_c=C(2);
fixrad = fix(rad);
rowrange = fix(cen_r-rad:cen_r+rad);
colrange = fix(cen_c-rad:cen_c+rad);
sel_r = [];
sel_c = [];
[C,R]=meshgrid(-rad:rad,-rad:rad);
d=hypot(R,C);
sub=d<=rad;
[r,c]=find(sub);
% cm(rowrange,colrange)=sub;
r = r+rowrange(1);
c = c+colrange(1);
idx = r >0 & r<=Imgsize(1) & c >0 & c<=Imgsize(2);
cidx=sub2ind(Imgsize,r(idx),c(idx));
cm(cidx)=1;

% for r=rowrange
%     for c=colrange
%         if hypot(r-cen_r,c-cen_c)<=rad
% %             cm(r,c)=true;
%             if r > 0 && r <= Imgsize(1) && c > 0 && c <= Imgsize(2)
%                 sel_r=cat(2,sel_r,r);
%                 sel_c=cat(2,sel_c,c);
%             end
%         end
%     end
% end
% cidx = sub2ind(Imgsize, sel_r, sel_c);
% cm(cidx)=true;
% figure,imshow(cm)