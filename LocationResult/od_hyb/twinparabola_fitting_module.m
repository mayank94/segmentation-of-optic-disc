function [twin_parabola,major_parabola,minor_parabola,params_maj,params_min,miscr,miscl]=twinparabola_fitting_module(ves_skel,skel,mid,ws)
    lowr=mid(1)-fix(ws/2);
    lowc=mid(2)-fix(ws/2);
    upr=mid(1)+fix(ws/2);
    upc=mid(2)+fix(ws/2);
    if lowr<1
        lowr=1;
    end
    if lowc<1
        lowc=1;
    end
    if upr>size(ves_skel,1)
        upr=size(ves_skel,1);
    end
    if upc>size(ves_skel,2)
        upc=size(ves_skel,2);
    end
    window=[lowr,upr,lowc,upc];
    for l=1:15
        [rv,lv,res1,res2]=fitting_parabola_check(ves_skel,window);
        residuals1(l)=res1;
        residuals2(l)=res2;
        rvm(l,:)=rv;
        lvm(l,:)=lv;
        
    end
    flag=0;
    [mr,pr]=max(rvm(:,1));
    [ml,pl]=max(lvm(:,1));
    if mr>=ml
        flag=1;
        maxpar_imliers=mr;
        cent(1)=rvm(pr,2);
        cent(2)=rvm(pr,3);
        [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
        allpts = [X(:),Y(:)];
        params_maj=[cent(1), cent(2), rvm(pr,4), rvm(pr,5)];
        points1=parabola(params_maj,allpts./500);
        outfig1=zeros(size(ves_skel));
        outfig1(:)=points1;
        finout1=outfig1<0.002;
        residualmaj_value=residuals1(pr);
    else
        maxpar_imliers=ml;
        cent(1)=lvm(pl,2);
        cent(2)=lvm(pl,3);
        [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
        allpts = [X(:),Y(:)];
        params_maj=[cent(1), cent(2), lvm(pl,4), lvm(pl,5)];
        points1=parabola(params_maj,allpts./500);    
        outfig1=zeros(size(ves_skel));
        outfig1(:)=points1;
        finout1=outfig1<0.002;
        residualmaj_value=residuals2(pl);
    end
    inliers_maxratio=maxpar_imliers/fix(0.8*sum(ves_skel(:)));
    miscr=[inliers_maxratio,residualmaj_value];
    [center,rev_val,miscl]=fitting_parabolas_check(skel,cent,flag,finout1,window);
    if flag==1
        rval=rvm(pr,2:end);
        lval=[cent,rev_val];
        [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
        allpts = [X(:),Y(:)];
        points2=parabola(lval,allpts./500);
        outfig2=zeros(size(ves_skel));
        outfig2(:)=points2;
        params_min=lval;
        finout2=outfig2<0.002;
    else
        rval=[cent,rev_val];
        lval=lvm(pl,2:end);
        [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
        allpts = [X(:),Y(:)];
        points2=parabola(rval,allpts./500);
        outfig2=zeros(size(ves_skel));
        outfig2(:)=points2;
        params_min=rval;
        finout2=outfig2<0.002;
    end
    outfig=finout1|finout2;
    major_parabola=finout1;
    minor_parabola=finout2;
    twin_parabola=outfig;
    return
    