function [linimage_b,bestparams,linimage,linparams,hm,mv,rightval]=axis_determine_ransac(vess)
	skeleton=bwmorph(vess,'skel','Inf');
	% skeleton = vess;
	tmp=imfilter(uint8(skeleton),ones(3));
	% figure,imshow(tmp<3);
	skel_seg = skeleton & (tmp<4);

	cc1 = bwconncomp(skel_seg);
    reg=regionprops(cc1,'Area');
    reg=[reg.Area];
    arar=reg>5;
    cc.NumObjects=sum(arar);
    cc.PixelIdxList=cc1.PixelIdxList(arar);
	% figure,imshow(bwlabel(skel_seg),[])
    window=[1,1,size(skeleton,1),size(skeleton,2)];
    num_iter=2;
    trial=1;
    np = cc.NumObjects; %sum(skeleton(:));
    numinliers=[];
    Params = [];
    seg_sel = [];
    halfmask=false(size(skeleton));
    halfmask(:,1:fix(size(skeleton,2)/2))=1;
    ihalfmask=1-halfmask;
    while num_iter>trial
       pc=0.60; 
       seg_sel(trial,:) = randsample(1:np, fix(pc*np));
       skel_inp = false(size(skeleton));
       
       for si=seg_sel(trial,:)
           pix = cc.PixelIdxList{si};
           skel_inp(pix)=1;
       end
       npi=sum(skel_inp(:));
       [ellval,rightval]= fitting_ellipse_angcheck_v2(skel_inp,window,pc); 
       numinliers(trial)=ellval(1)/(pc*npi);
       Params(trial,:)=ellval(2:end);
       pnout=1-(numinliers(trial)^4); %(pc*np));
       pnout=max(eps,pnout);
       pnout=min(1-eps,pnout);
       num_iter=log(0.01)/log(pnout);
       trial=trial+1;
       if trial>2*np
           break
       end
    end
    [mv,mi]=max(numinliers);

    
    bestparams = Params(mi,:);
% params=ellval(2:end);
linimage=zeros(size(vess));

[Y,X]=meshgrid(1:size(vess,2),1:size(vess,1));

allpts = [X(:),Y(:)];   % {[r,c]}
slope = tan(bestparams(3));
x0 = bestparams(1)*500;
y0 = bestparams(2)*500;
aval=bestparams(4)*500;
bval=bestparams(5)*500;
% value=allpts(:,1)-tan(-(params(3)-pi/2))*(allpts(:,2)-y0)+x0;
% value = allpts(:,1)-((allpts(:,2)-y0)/slope+x0);
value = allpts(:,2)-y0-slope*(allpts(:,1)-x0);

linimage(:)=value;

if ~true
    
    figure(1),    
    subplot(3,4,4),imshow(vess>0 | abs(linimage)<3)
end

if true
    bestvf = ellipse(bestparams, allpts./500);   
    
    skel_inp = zeros(size(skeleton));
    skel_inp1 = zeros(size(skeleton));
    skel_inp(:)=bestvf<0.002;
    skel_inp1(:)=bestvf;
    
    linimage_b=linimage>0;
    linparams = [x0, y0, slope, aval, bval];
    hm=ones(size(linimage));
%     [hm,out_vessum]=determine_vessel_pixelssum(skel_inp1,linparams,vess);
   npi=0; 
    for si=seg_sel(mi,:)
       pix = cc.PixelIdxList{si};
       skel_inp(pix)=1;
       npi=npi+numel(pix);
   end
   
%     vp= ellipse(params1, allpts./500);
%     figure(1)
%     subplot(3,4,6),imshow(skel_inp,[]), 
%     title(sprintf('inliers %d/%d', fix(numinliers(mi)*npi),npi) )
           
end


