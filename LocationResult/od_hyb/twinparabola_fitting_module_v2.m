function [twin_parabola,major_parabola,minor_parabola,params_maj,params_min,miscmaj,miscmin]=twinparabola_fitting_module_v2(ves_skel,skel,ws)
   
    window=ws;
    
    tmp=imfilter(uint8(ves_skel),ones(3));
	% figure,imshow(tmp<3);
	skel_seg = ves_skel & (tmp<4);

	cc1 = bwconncomp(skel_seg);
    reg=regionprops(cc1,'Area');
    reg=[reg.Area];
    arar=reg>15;
    cc.NumObjects=sum(arar);
    cc.PixelIdxList=cc1.PixelIdxList(arar);
    
    num_iter=2;
    trial=1;
    np = cc.NumObjects; %sum(skeleton(:));
    numinliersr=[];
    Paramsr = [];
    numinliersl=[];
    Paramsl = [];
    seg_sel = [];
    
    while num_iter>trial
       pc=0.70; 
       seg_sel(trial,:) = randsample(1:np, fix(pc*np));
       skel_inp = false(size(ves_skel));
       
       for si=seg_sel(trial,:)
           pix = cc.PixelIdxList{si};
           skel_inp(pix)=1;
       end
       npi=sum(skel_inp(:));
       [rv,lv,res1,res2]= fitting_parabola_check_v2(skel_inp,window,pc); 
       numinliersr(trial)=rv(1)/npi;
       Paramsr(trial,:)=rv(2:end);
	   residualsr(trial)=res1;
       
       numinliersl(trial)=lv(1)/npi;
       Paramsl(trial,:)=lv(2:end);
	   residualsl(trial)=res2;
       
       numinliers1=max(numinliersr,numinliersl);
       pnout=1-(numinliers1(trial)^4); %(pc*np));
       pnout=max(eps,pnout);
       pnout=min(1-eps,pnout);
       num_iter=log(0.01)/log(pnout);
       trial=trial+1;
       if trial>2*np %*pc
           break
       end
    end
    
    rvm=[numinliersr',Paramsr];
    lvm=[numinliersl',Paramsl];
    
%     [mv,mi]=max(numinliers);
%     bestparams = Params(mi,:);
    
    flag=0;
    [mr,pr]=max(rvm(:,1));
    [ml,pl]=max(lvm(:,1));
    if mr>=ml
        flag=1;
        maxpar_imliers=mr;
        cent(1)=rvm(pr,2);
        cent(2)=rvm(pr,3);
        [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
        allpts = [X(:),Y(:)];
        params_maj=[cent(1), cent(2), rvm(pr,4), rvm(pr,5)];
        points1=parabola(params_maj,allpts./500);
        outfig1=zeros(size(ves_skel));
        outfig1(:)=points1;
        finout1=outfig1<0.002;
        residualmaj_value=residualsr(pr);
    else
        maxpar_imliers=ml;
        cent(1)=lvm(pl,2);
        cent(2)=lvm(pl,3);
        [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
        allpts = [X(:),Y(:)];
        params_maj=[cent(1), cent(2), lvm(pl,4), lvm(pl,5)];
        points1=parabola(params_maj,allpts./500);    
        outfig1=zeros(size(ves_skel));
        outfig1(:)=points1;
        finout1=outfig1<0.002;
        residualmaj_value=residualsl(pl);
    end
    inliers_maxratio=maxpar_imliers;
    miscmaj=[inliers_maxratio,residualmaj_value];
    
    num_iter=2;
    trial=1;
   
    numinliers2=[];
    Params2 = [];
    seg_sel = [];
    while num_iter>trial
       pc=1; 
       seg_sel(trial,:) = randsample(1:np, fix(pc*np));
       skel_inp = false(size(ves_skel));
       
       for si=seg_sel(trial,:)
           pix = cc.PixelIdxList{si};
           skel_inp(pix)=1;
       end
       npi=sum(skel_inp(:));
       [rev_val,miscmin,minpar_imliers]= fitting_parabolas_check_v2(skel,cent,flag); 
       numinliers2(trial)=minpar_imliers/npi;
       Params2(trial,:)=rev_val;
	   residuals2(trial)=miscmin(2);
       
       pnout=1-(numinliers2(trial)^4); %(pc*np));
       pnout=max(eps,pnout);
       pnout=min(1-eps,pnout);
       num_iter=log(0.01)/log(pnout);
       trial=trial+1;
       if trial>2*np*pc
           break
       end
    end
    [mv,mi]=max(numinliers2);
    bestparams = Params2(mi,:);
    miscmin=[mv,residuals2(mi)];
    
%     [rev_val,miscmin,minpar_imliers]=fitting_parabolas_check_v2(skel,cent,flag);
    if flag==1
        rval=rvm(pr,2:end);
        lval=[cent,bestparams];
        [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
        allpts = [X(:),Y(:)];
        points2=parabola(lval,allpts./500);
        outfig2=zeros(size(ves_skel));
        outfig2(:)=points2;
        params_min=lval;
        finout2=outfig2<0.002;
    else
        rval=[cent,bestparams];
        lval=lvm(pl,2:end);
        [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
        allpts = [X(:),Y(:)];
        points2=parabola(rval,allpts./500);
        outfig2=zeros(size(ves_skel));
        outfig2(:)=points2;
        params_min=rval;
        finout2=outfig2<0.002;
    end
    
    outfig=finout1|finout2;
    major_parabola=finout1;
    minor_parabola=finout2;
    twin_parabola=outfig;
    return
    