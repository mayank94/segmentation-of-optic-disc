
function [right_val,left_val]= fitting_parabola(outskel)
    
    imsiz = size(outskel); % nr,nc : x=r , y=c, org = topleft
    hsiz = fix(imsiz./2);
    [Y,X]=meshgrid(1:imsiz(2),1:imsiz(1));

    allpts = [X(:),Y(:)];      

     
    figure(1),
    subplot(3,2,1),
    imshow(outskel,[]), title('target')
    
    init = [hsiz./500, 90*pi/180, 500/4/500];
    
    initvf = parabola(init, allpts./500);
    
    tmp2=zeros(imsiz);
    tmp2(:)=initvf<0.02;

    figure(1)
    subplot(3,2,2), imshow(tmp2,[]), title('initialization');
    
    
    
     
    [pts_x, pts_y] = find(outskel);
    
    ptratio=fix(0.8*numel(pts_x));
    sampleidx=randsample(numel(pts_x),ptratio);
    sel_tr=pts_x(sampleidx);
    sel_tc=pts_y(sampleidx);
    sel= [sel_tr, sel_tc];
    inpts = [sel_tr, sel_tc]./500; 

    inval = zeros(size(inpts,1),1);
    
    lb1 = [1/500, 1/500  , (90-10)*pi/180, 120/4/500];
    ub1 = [hsiz(1)*2/500, hsiz(2)*2/500, (90+10)*pi/180, 640/4/500]; 
%     inp=inpts*500;
%     lb1 = [1, 1 , (90-10)*pi/180, 120/4];
%     ub1 = [hsiz(1)*2, hsiz(2)*2, (90+10)*pi/180, 500/4]; 
%     init1=[hsiz, 90*pi/180, 500/4];
    [params1, resnorm1, residual1, exitflag1] = lsqcurvefit(@parabola, init, inpts, inval, lb1, ub1);
    rightar=residual1<0.013;
    num_inliers1=sum(rightar);
    vp= parabola(params1, allpts./500);
    tmp3 = zeros(imsiz);
    tmp3(:)=vp<0.003;
    right_val=[num_inliers1, params1];
    figure(1),
    subplot(3,2,3),imshow(tmp3,[]), title('fitted-right')
    
    
    lb2 = [1/500, 1/500  , (-90-10)*pi/180, 120/4/500];
    ub2 = [hsiz(1)*2/500, hsiz(2)*2/500, (-90+10)*pi/180, 640/4/500];
%     inp=inpts*500;
%     lb2 = [1, 1 , (-90-10)*pi/180, 120/4];
%     ub2 = [hsiz(1)*2, hsiz(2)*2, (-90+10)*pi/180, 500/4]; 
%     init1=[hsiz, 90*pi/180, 500/4];
        
    [params2, resnorm2, residual2, exitflag2] = lsqcurvefit(@parabola, init, inpts, inval, lb2, ub2);
    rightar=residual2<0.013;
    num_inliers2=sum(rightar);
    vp= parabola(params2, allpts./500);
    tmp4 = zeros(imsiz);
    tmp4(:)=vp<0.003;
    left_val=[num_inliers2, params2];
    figure(1),
    subplot(3,2,4),imshow(tmp4,[]), title('fitted-left')
    
    tmp5=tmp3+tmp4;
    figure(1),
    subplot(3,2,5),imshow(tmp5,[]), title('Double-arcade')
    
    
%     disp('vaanu')
    
%--- % % % % % % % % % % % % 

function v = parabola(args,pts)

pts_x = pts(:,1)-args(1);
pts_y = pts(:,2)-args(2);

before = [pts_x, pts_y]';

c = cos(-args(3));
s = sin(-args(3));


pts_rot = [c, -s; s, c] * before;

v = (pts_rot(2,:)).^2 - 4*args(4).*(pts_rot(1,:));
tm=zeros(size(outskel));
for re=1:size(inpts,1)
    tm(inpts(re,1)*500,inpts(re,2)*500)=1;
end
% [Y,X]=meshgrid(1:imsiz(2),1:imsiz(1));
% 
%     allpts = [X(:),Y(:)]; 
tmp0=zeros(size(outskel));
tmp0(:)=v<0.002;
parskel=bwmorph(tmp0,'skel','Inf');
% pardist=bwdist(parskel);
vesdist=bwdist(tm);
% multdist=im2double(pardist).*im2double(vesdist);
val=sqrt(sum(vesdist(tmp0))/size(inpts,1));

v=abs(v)'
v=val.*ones(size(inpts,1),1);
