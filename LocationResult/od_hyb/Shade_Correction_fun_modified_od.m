function S = Shade_Correction_fun_modified_od(im,fmask,scale,flag)%fmask here is inverted fundus mask
% fmask = im < (0.04 * max(im(:)));
fmask = im2uint8(fmask);
% fmask(fmask == 1) = 255;
%% Shade Correction
if (scale >= 0.125&& flag == 0)
    fun = @(x)medfilt2(x,[41 41],'symmetric');% XXX: was 21,21 for
   %%%1/16,what if I just use one median filtering size rather than 
elseif(scale < 0.125 && flag == 0)
   fun = @(x)medfilt2(x,[21 21],'symmetric'); 
end

if(flag == 1)
    fun = @(x)medfilt2(x,[61 61],'symmetric');
end
fmask_dash = imcomplement(fmask);
background_im = roifilt2(im,fmask_dash,fun);
background_im = mat2gray(background_im);
im = mat2gray(im);
fmask = mat2gray(fmask);

fmask_dash = mat2gray(fmask_dash);


S_dash = (im-background_im);
fmask = fmask > 0;
fmask_dash = fmask_dash > 0;

S_dash(fmask)=0;
S_dash = S_dash ./background_im;

nan_locations=isinf(S_dash) | isnan(S_dash);
S_dash(nan_locations)=1;


std_S_dash = std2(S_dash);
S = S_dash/std_S_dash;
S(fmask)=0;
S(S>1)=1;
end
