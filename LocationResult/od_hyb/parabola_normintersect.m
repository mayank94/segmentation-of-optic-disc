function [sumimage,gf]=parabola_normintersect(contour,linparams,vdist)

bw_im=abs(contour)<0.002;
skelim=bwmorph(bw_im,'skel','Inf');

[ou,wh,direct,angles]=FrangiFilter2D_v(contour);

orient=tan((angles).*skelim);

x0=linparams(1)*500;
y0=linparams(2)*500;
slope=tan(linparams(3));
aval=linparams(4)*500;
[Y,X]=meshgrid(1:size(skelim,2),1:size(skelim,1));
    
pts=[X(:),Y(:)];

slope=-(1/slope);
valuelin = pts(:,2)-y0-slope*(pts(:,1)-x0);
leftlinimage=zeros(size(skelim));
leftlinimage(:)=abs(valuelin)<0.02;

distim=zeros(size(skelim));
distv = abs((slope*pts(:,1)-pts(:,2))+(y0-slope*x0))./hypot(slope,1);
distim(:)=distv;
maskim=distim<=2*aval;
% reqarea=~reqarea;

% maskim=leftlinimage&reqarea;

partpar=skelim&maskim;
partor=orient.*maskim;

[X,Y]=meshgrid(1:size(skelim,2),1:size(skelim,1));
normim=zeros(size(skelim));
pts=[X(:),Y(:)];

sum_vess=[];
norm_stack=[];
pars=bwmorph(partpar,'spur',1);
eppar=partpar-pars;
[r,c]=find(eppar==1);
if numel(r)==0
    gf=[1,1];
    sumimage=zeros(size(skelim));
    return;
end
% epts=[r,c];
% c1=find(partpar(fix(x0),:)==1);
% pt=[fix(x0),c1];
% vals=[pt;epts];
% r=vals(:,1);
% c=vals(:,2);
ylab=zeros(numel(r),1);
cumnorm=zeros(size(vdist));
for id=1:numel(r)
    slope=partor(r(id),c(id));
    value = pts(:,2)-r(id)-slope*(pts(:,1)-c(id));
    normim(:)=abs(value)<5;
    xmat(id,:)=[1, -slope];
    ylab(id)=(c(id)-r(id));
    distnorm=im2double(vdist).*normim;
    
%     figure(3),imshow(double(normim)+double(skelim),[]);
%     hold on 
%     plot(c(id),r(id),'rx')
%     hold off
%     colormap jet
%     pause
    cumnorm=distnorm+cumnorm;
    norm_stack(:,:,id)=normim;
    
end
% coordinates=pinv(xmat)*ylab;
% coordinates=flipud(coordinates);
sumimage=sum(norm_stack,3); 
[rf,cf]=find(sumimage==max(sumimage(:)));
gf(1)=median(rf);
gf(2)=median(cf);
% val=[gf;coordinates'];


