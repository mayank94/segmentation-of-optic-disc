function [twin_parabola,major_parabola,minor_parabola,params_maj,params_min,miscr,miscl]=twinparabola_fitting_module_ransac(ves_skel,skel,mid,ws, init)
    lowr=mid(1)-fix(ws/2);
    lowc=mid(2)-fix(ws/2);
    upr=mid(1)+fix(ws/2);
    upc=mid(2)+fix(ws/2);
    if lowr<1
        lowr=1;
    end
    if lowc<1
        lowc=1;
    end
    if upr>size(ves_skel,1)
        upr=size(ves_skel,1);
    end
    if upc>size(ves_skel,2)
        upc=size(ves_skel,2);
    end
    window=[lowr,upr,lowc,upc];
	
	tmp=imfilter(uint8(ves_skel),ones(3));
	% figure,imshow(tmp<3);
	skel_seg = ves_skel & (tmp<4);

	cc = bwconncomp(skel_seg);

	% figure,imshow(bwlabel(skel_seg),[])
    window=[1,size(ves_skel,1),1,size(ves_skel,2)];
    num_iter=2;
    trial=1;
    np = cc.NumObjects; %sum(skeleton(:));
    numinliers=[];
    Params = [];
    seg_sel = [];
    
    while num_iter>trial
       pc=0.30; 
       seg_sel(trial,:) = randsample(1:np, fix(pc*np));
       skel_inp = false(size(ves_skel));
       
       for si=seg_sel(trial,:)
           pix = cc.PixelIdxList{si};
           skel_inp(pix)=1;
       end
       npi=sum(skel_inp(:));
       [rv,lv,resnorm1,resnorm2]= fitting_parabola_check_segments(skel_inp,window,init); 
       numinliersr(trial)=rv(1)/(pc*npi);
       Paramsr(trial,:)=rv(2:end);
	   numinliersl(trial)=lv(1)/(pc*npi);
       Paramsl(trial,:)=lv(2:end);
	   residualsr(trial)=resnorm1;
	   residualsl(trial)=resnorm2;
       pnout=1-(numinliers(trial)^4); %(pc*np));
       pnout=max(eps,pnout);
       pnout=min(1-eps,pnout);
       num_iter=log(0.01)/log(pnout);
       trial=trial+1;
       if trial>2*np
           break
       end
    end
	rvm=[numinliersr,Paramsr];
	lvm=[numinliersl,Paramsl];
    % [mv,mi]=max(numinliers);
    % bestparams = Params(mi,:);
	
    % for l=1:15
        % [rv,lv,res1,res2]=fitting_parabola_check(ves_skel,window,init);
        % residuals1(l)=res1;
        % residuals2(l)=res2;
        % rvm(l,:)=rv;
        % lvm(l,:)=lv;
        
    % end
    flag=0;
    [mr,pr]=max(rvm(:,1));
    [ml,pl]=max(lvm(:,1));
    if mr>=ml
        flag=1;
        maxpar_imliers=mr;
        cent(1)=rvm(pr,2);
        cent(2)=rvm(pr,3);
        [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
        allpts = [X(:),Y(:)];
        params_maj=[cent(1), cent(2), rvm(pr,4), rvm(pr,5)];
        points1=parabola(params_maj,allpts./500);
        outfig1=zeros(size(ves_skel));
        outfig1(:)=points1;
        finout1=outfig1<0.002;
        residualmaj_value=residuals1(pr);
    else
        maxpar_imliers=ml;
        cent(1)=lvm(pl,2);
        cent(2)=lvm(pl,3);
        [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
        allpts = [X(:),Y(:)];
        params_maj=[cent(1), cent(2), lvm(pl,4), lvm(pl,5)];
        points1=parabola(params_maj,allpts./500);    
        outfig1=zeros(size(ves_skel));
        outfig1(:)=points1;
        finout1=outfig1<0.002;
        residualmaj_value=residuals2(pl);
    end
    inliers_maxratio=maxpar_imliers/fix(0.8*sum(ves_skel(:)));
    miscr=[inliers_maxratio,residualmaj_value];
    [center,rev_val,miscl]=fitting_parabolas_check(skel,cent,flag,finout1,window);
    if flag==1
        rval=rvm(pr,2:end);
        lval=[cent,rev_val];
        [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
        allpts = [X(:),Y(:)];
        points2=parabola(lval,allpts./500);
        outfig2=zeros(size(ves_skel));
        outfig2(:)=points2;
        params_min=lval;
        finout2=outfig2<0.002;
    else
        rval=[cent,rev_val];
        lval=lvm(pl,2:end);
        [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
        allpts = [X(:),Y(:)];
        points2=parabola(rval,allpts./500);
        outfig2=zeros(size(ves_skel));
        outfig2(:)=points2;
        params_min=rval;
        finout2=outfig2<0.002;
    end
    outfig=finout1|finout2;
    major_parabola=finout1;
    minor_parabola=finout2;
    twin_parabola=outfig;
    return
    