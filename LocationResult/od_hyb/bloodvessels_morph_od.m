function vessels_recons = bloodvessels_morph_od(ipp,fm,debug)

if nargin<3, debug=false; end
if isa(ipp,'double')
    ipp = uint8(255.*ipp);
end

%[ipp_m,thr] = domedianfilt(ipp);
%darkobjs = ipp_m - ipp; %positive for dark regions, negative for bright

ipp2=subtractive(ipp,fm);

if debug, 
    figure,imshow(ipp2),title('subtractive corrected')%,pixval on
%     pause
end

darkobjs=imcomplement(ipp2);

darkobjs=imfilter(double(darkobjs),fspecial('gaussian',11,1.2),'conv','same');
% darkobjs=uint8(255*mat2gray(darkobjs));
if debug, 
    figure,imshow(darkobjs),title('darkobjs'),colormap hot
end
%pause

imthat=mytophat(darkobjs);
% imthat = darkobjs;

if debug,
    figure,imshow(imthat),title('t-hat')
end
%class(imthat)
%pause

vessels = getvessels_soft(imthat);
if debug,
    figure,imshow(vessels,[]),title('vessels')
end
%pause

vessels_recons = imreconstruct(vessels,imthat,8);
if debug
    figure, imshow(vessels_recons,[]),title('vessels\_recon')    
end
%pause
return;

candimg1 = double(imthat)-double(vessels_recons);

fm([1:5,end-5:end],:)=0;
fm(:,[1:5,end-5:end])=0;

candimg1 = candimg1 .* fm;

% candimg_soft1 = imadjust(candimg1, stretchlim(candimg1),[],0.5);

candimg_soft1 = uint8(255*mat2gray(double(candimg1))); %stretch to [0,255]
%%%%candimg_soft = medfilt2(candimg_soft,[3 3]); %remove extrema?

if debug
    figure, imshow(candimg_soft1),colormap hot,title('candsoft1'),%pixval on
    disp([ mfilename ' paused pak...'])
    %pause
end
%pause
 
% candimg2 = double(imthat)-double(vessels);
% candimg_soft2 = uint8(255*mat2gray(candimg2,[0 255]));
% 
% figure, imshow(candimg_soft2),colormap hot,title('candsoft2'),pixval on
% pause

candimg_soft=candimg_soft1;
% thr=50;

%------------------------------------------------
function imthat=mytophat(darkobjs)

se1=strel('disk',8);
ime=imerode(darkobjs,se1);
% figure,imshow(ime),title('eroded')
% pause

se2=strel('disk',10);
imed=imdilate(ime,se2);
%figure,imshow(imed),title('dilated')
%pause

% imthat=imsubtract(darkobjs,imde); 
imthat=double(darkobjs)-double(imed);
mM=minmax(imthat(:)');
imthat=uint8(255* (imthat-mM(1))/(mM(2)-mM(1)));

%------------------------------------------------
% function vessel2 = dogvessel(imthat)
% f=mydog();
% out=conv2(double(imthat),f,'same');
% 
% vessel2=out.*(out<0);

%------------------------------------------------
function darkobjs2 = applydogfilter(imthat)

f=mydog();
darkobjs2 = conv2(double(imthat),f,'same');

mM=minmax(darkobjs2(:)');

darkobjs2 = darkobjs2.*(darkobjs2>0);


%------------------------------------------------
function [ipp_m,thr] = domedianfilt(ipp)

%% Customised median filtering for ROC dataset
[row col]=size(ipp);
if row>1000 & col>1000
    ipp_m= medfilt2(ipp,[95 95]);
    %candimg= (ipp_m-ipp);
    %candimg= medfilt2(candimg,[3 3]);
    thr=55;
    %disp(['thres is:  ' num2str(thr)])
end

if row>500 & row<=1000 & col>500 & col<=1000
    ipp_m= medfilt2(ipp,[53 53]);
    %candimg= (ipp_m-ipp);
    %candimg= medfilt2(candimg,[3 3]);
    thr=55;
    %disp(['thres is:  ' num2str(thr)])
end

%------------------------------------------------
function out = getvessels_soft3(darkobjs)

se=strel('disk',15);
maxdil=imdilate(darkobjs,se);

imshow(maxdil,[]),title('maxdil')
pause

se=strel('disk',6);
out=imerode(maxdil,se);

%------------------------------------------------
function out = getvessels_soft2(darkobjs)

c=class(darkobjs);
maxdil=zeros(size(darkobjs));
maxdil=eval([c '(maxdil)']);

for i=1:8
    se=strel('line',20,(i-1)*180/8);
    dil_i=imdilate(darkobjs,se);
    maxdil=max(maxdil,dil_i);
end

imshow(maxdil,[]),title('maxdil')
pause

out=imerode(maxdil,strel('disk',6));


%------------------------------------------------
function maxopen = getvessels_soft(darkobjs)

% c=class(darkobjs);
maxopen=zeros(size(darkobjs),class(darkobjs));
% maxopen=eval([c '(maxopen)']);

for i=1:8
   se=strel('line',15,(i-1)*180/8);
   op=imopen(darkobjs,se);
   maxopen=max(maxopen,op);
end

