function [centmacell,centmacpar]=mac_detection_pinpoint(ch,majp,params,vmap)
vdist=bwdist(vmap);
vdist=vdist-min(vdist(:));
vdist=vdist./max(vdist(:));

chmed=medfilt2(im2uint8(ch),[25 25]);       

macbox=false(size(ch));
mbox=make_box(params(1)*500,params(2)*500,80,ch);
macbox(mbox(1):mbox(2),mbox(3):mbox(4))=1;
chmed1=im2double(chmed).*macbox;
[rmed,cmed]=find(chmed1==min(chmed1(macbox)));
medmat=[rmed,cmed];
if numel(rmed)>1
    centmacell=fix(median(medmat));
else
    centmacell=medmat;
end

parimg=zeros(size(ch));
[Y,X]=meshgrid(1:size(ch,2),1:size(ch,1));
pts=[X(:),Y(:)];
val=parabola(majp,pts./500);
parimg(:)=val;
[sumimage,value]=parabola_normintersect(parimg,majp,vdist);
centmacpar=[value(1,1),value(1,2)];

