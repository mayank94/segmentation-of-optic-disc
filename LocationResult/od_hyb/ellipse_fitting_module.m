function [ell_out,params,misc]=ellipse_fitting_module(ves_skel,mid,ws)
    lowr=mid(1)-fix(ws/2);
    lowc=mid(2)-fix(ws/2);
    upre=mid(1)+fix(ws/2);
    upc=mid(2)+fix(ws/2);
    if lowr<1
        lowr=1;
    end
    if lowc<1
        lowc=1;   
    end

    if upre>size(ves_skel,1)
        upre=size(ves_skel,1);
    end
    if upc>size(ves_skel,2)
        upc=size(ves_skel,2)
    end
    window=[1, 1, size(ves_skel,1), size(ves_skel,2)];
    for l=1:15
        [ev,res]=fitting_ellipse_check(ves_skel,window);
        residuals(l)=res;
        evm(l,:)=ev;        
        
    end
%     flag=0;
    [me,pe]=max(evm(:,1));
%     [mle,ple]=max(levm(:,1));
%     if mre>=mle
%         flag=1;
        max_inliers=me;
        cent(1)=evm(pe,2);
        cent(2)=evm(pe,3);
        [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
        allpts = [X(:),Y(:)];
        params=[cent(1), cent(2), evm(pe,4), evm(pe,5), evm(pe,6)];
        points1=ellipse(params,allpts./500);
        outfig1=zeros(size(ves_skel));
        outfig1(:)=points1;
        finout1=outfig1<0.002;
        inliers_ratio=max_inliers/sum(ves_skel(:));
        residual_val=residuals(pe);
        misc=[inliers_ratio,residual_val];
%     else
%         maxpar_imliers=mle;
%         cent(1)=levm(ple,2);
%         cent(2)=levm(ple,3);
%         [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
%         allpts = [X(:),Y(:)];
%         params=[cent(1), cent(2), levm(ple,4), levm(ple,5), levm(ple,6)];
%         points1=ellipse(params,allpts./500);    
%         outfig1=zeros(size(ves_skel));
%         outfig1(:)=points1;
%         finout1=outfig1<0.002;
%     end

%     [center,rev_val,minpar_inliers]=fitting_parabolas_check(ves_skel,cent,flag,finout1,window);
%     if flag==1
%         rval=revm(pre,2:end);
%         lval=[cent,rev_val];
%         [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
%         allpts = [X(:),Y(:)];
%         points2=parabola(lval,allpts./500);
%         outfig2=zeros(size(ves_skel));
%         outfig2(:)=points2;
%         params_min=lval;
%         finout2=outfig2<0.002;
%     else
%         rval=[cent,rev_val];
%         lval=levm(ple,2:end);
%         [Y,X]=meshgrid(1:size(ves_skel,2),1:size(ves_skel,1));
%         allpts = [X(:),Y(:)];
%         points2=parabola(rval,allpts./500);
%         outfig2=zeros(size(ves_skel));
%         outfig2(:)=points2;
%         params_min=rval;
%         finout2=outfig2<0.002;
%     end
%     outfig=finout1|finout2;
    ell_out=finout1;
%     figure(1),subplot(3,2,6),imshow(ell_out,[]), title('fitted-ellipse')
%     minor_parabola=finout2;
%     twin_parabola=outfig;
    return
    