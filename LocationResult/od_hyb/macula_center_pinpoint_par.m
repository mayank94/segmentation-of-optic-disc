clc;
% close all;
dirnames={'odmac_prefinal_diaretdb1',...
'odmac_prefinal_dmed','odmac_prefinal_messidor',... 
'odmac_prefinal_drive','odmac_prefinal_sel_ou'};

dirnames1={'full_OD/diaretdb1_sel',...
'full_OD/dmed_sel','full_OD/messidor_sel',... 
'full_OD/drive_sel','full_OD/sel_out_80'};
load('randtreedefault.mat');

for di=1:numel(dirnames)
    dirname = dirnames{di};
    dirname1= dirnames1{di};
%     xpng=dir([dirname '/*.png']);
%     xtif = dir([dirname '/*.tif']);
%     xjpg = dir([dirname '/*.jpg']);
%     x = cat(1,xpng,xtif,xjpg);
    x=dir([dirname '/*.mat']);
    outdir=['maccenter_' dirname(14:end)];
    if ~exist(outdir,'dir')
        mkdir(outdir)
    end
    maxim=numel(x);

    for u=1:maxim
        try
            load(sprintf('%s/%s',dirname,x(u).name));
            inp=imread(sprintf('%s/%s',dirname1,x(u).name(1:(end-4))));
        catch
            continue;
        end
        origsiz = [size(inp,1),size(inp,2)];
        factor=fix(origsiz(1)/256);
        ch=green_org;
        chmed=medfilt2(im2uint8(ch),[25 25]);
        imr = imresize(inp,1/factor);

        rd = imr(:,:,1);
        gr = imr(:,:,2);
        bl = imr(:,:,3);

        macbox=false(size(green_org));
        mbox=make_box(params(1)*500,params(2)*500,80,green_org);
        macbox(mbox(1):mbox(2),mbox(3):mbox(4))=1;
        chmed1=im2double(chmed).*macbox;
        [rmed,cmed]=find(chmed1==min(chmed1(macbox)));
        medmat=[rmed,cmed];
        if numel(rmed)>1
            centmacell=fix(median(medmat));
        else
            centmacell=medmat;
        end
        
        
        parimg=zeros(size(green_org));
        [Y,X]=meshgrid(1:size(green_org,2),1:size(green_org,1));
        pts=[X(:),Y(:)];
        val=parabola(majp,pts./500);
        parimg(:)=val;
        [sumimage,value]=parabola_normintersect(parimg,majp);
        centmacpar=[value(1,1),value(1,2)];
        
        scrap=false(size(ch));
        scrap(centmacpar(1),centmacpar(2))=1;
        scrap=imdilate(scrap,strel('square',6));
        
        scrap1=false(size(ch));
        scrap1(centmacell(1),centmacell(2))=1;
        scrap1=imdilate(scrap1,strel('square',6));
        scrap1=bwmorph(scrap1,'remove');
        sccrap1=imdilate(scrap1,strel('square',3));
        
        greenmaccent=ch;
        greenmaccent(scrap)=0;
        greenmaccent(scrap1)=0;
        
        
        imwrite(greenmaccent,sprintf('%s/%s',outdir,x(u).name(1:(end-4))));
        save(sprintf('%s/%s.mat',outdir,x(u).name(1:(end-4))),'centmacpar','centmacell');
    end
end
