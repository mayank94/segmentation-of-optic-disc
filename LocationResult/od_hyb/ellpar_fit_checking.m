function [flag]=ellpar_fit_checking(majp,params,miscmaj,miscmin,misce)       
        
focaldist=sqrt((params(4)*500)^2-(params(5)*500)^2);

xve1=params(1)*500+(params(4)*500*cos(-params(3)));
yve1=params(2)*500-(params(4)*500*sin(-params(3)));

xve2=params(1)*500-(params(4)*500*cos(-params(3)));
yve2=params(2)*500+(params(4)*500*sin(-params(3)));

xfe1=params(1)*500+(focaldist*cos(-params(3)));
yfe1=params(2)*500-(focaldist*sin(-params(3)));

xfe2=params(1)*500-(focaldist*cos(-params(3)));
yfe2=params(2)*500+(focaldist*sin(-params(3)));

ellv1=[yve1,xve1];ellv2=[yve2,xve2];ellf1=[yfe1,xfe1];ellf2=[yfe2,xfe2];
cent=[params(1)*500,params(2)*500];

parv=[majp(1)*500,majp(2)*500];
pltpts=[ellv1;ellv2;ellf1;ellf2;fliplr(cent);fliplr(parv)]; 
distv1=hypot(ellv1(2)-parv(1),ellv1(1)-parv(2));
distv2=hypot(ellv2(2)-parv(1),ellv2(1)-parv(2));
distf1=hypot(ellf1(2)-parv(1),ellf1(1)-parv(2));
distf2=hypot(ellf2(2)-parv(1),ellf2(1)-parv(2));
distc=hypot(cent(1)-parv(1),cent(2)-parv(2));
majorlen=params(4)*500;
minorlen=params(5)*500;
flag=1;
if max(miscmaj(1),miscmin(1))>0.001
    if misce(1)<0.0001|| majorlen > 2*minorlen
        flag=0;
    else    
        if min(distv1,distv2)<distc
            flag=1;
        else
            flag=0;
        end
    end
else
    flag=1;
end
        
    



    