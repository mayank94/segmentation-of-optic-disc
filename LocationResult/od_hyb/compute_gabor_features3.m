function [SmallScaleRes] = compute_gabor_features3(inputImage1,fmask,tau_array)

f=inputImage1;
original_img=f;
% f=double(f);
% f=f(:,:,2);%% uncomment it
f=imcomplement(f);
inputImage1=adapthisteq(f);%%
SmallScaleRes = [];
fdim=24;   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
inputImage=inputImage1;


% fast gabor filter code; rotating the base filter; use 4 scales and 60 angles; RECOMMENDED

% %  
 tic

%   ms=[48,60];
 
 
% load('outIm.mat');

%  tau_array=[1 2 5 8 10 12 16 18];
% tau_array=[3 5 8 10 12];%%
sigma_array = tau_array./(2*sqrt(2*log(2)));
gc=zeros(size(inputImage,1),size(inputImage,2),length(tau_array));

orientation = zeros(size(inputImage,1),size(inputImage,2),length(tau_array));

tau = 1;


 for tau_num = 1:1:length(tau_array)
     tau = tau_array(tau_num);
    tau,
    ell = 3;
    sigma_x = tau/(2*sqrt(2*log(2)));
   sigma_y = ell*sigma_x;
    sze1 = round(3*(sigma_x));
    sze2 = round(2*(sigma_y));
    [x,y] = meshgrid(-sze1:sze1,-sze2:sze2);
    u=(2*pi*sigma_x*sigma_y);
    gabor_matrix = exp(-(x.^2/sigma_x^2 + y.^2/sigma_y^2)/2).*cos(2*pi*(1/tau)*x);
    gabor_matrix=gabor_matrix./u;
    fun_original = gabor_matrix;
%     fun = imrotate(fun_original,-90,'bicubic','loose');
%     %fun = fun/sum(fun(:));
%     fun1 = @(x)imfilter(x,fun);
%     num_rotations= 180;%60
%     outputImage = roifilt2(inputImage1,ones(size(inputImage1)),fun1);
%     outputImage = roifilt2(inputImage1,fmask,fun1);
%     outputImage_buffer=zeros(size(inputImage,1),size(inputImage,2),2);
%     outputImage_buffer(:,:,1) = outputImage;
    count=0;
    theta1=[];
%     num_rotations=12;
    num_rotations=60;
      for theta=-90 +(180/num_rotations):180/num_rotations:90
          count=count+1;
          theta1=[theta1;theta];
       fun = imrotate(fun_original,theta,'bicubic','loose');
%         figure(5),surf(fun)        
        %pause(1)
%         fun = fun/sum(fun(:));
        fun1 = @(x)imfilter(x,fun);
        inputImage = roifilt2(double(inputImage1),fmask,fun1);
        outputImage_buffer(:,:,length(theta1)) = inputImage;
       
%         outputImage_buffer(:,:,1) = maxImage_buffer;
      end
%       if(tau_num == 1)
       SmallScaleRes = outputImage_buffer;
%       end
      
%      [maxImage_buffer,ind] = max(outputImage_buffer,[],3);
%     gc(:,:,tau_num)=maxImage_buffer.*fmask;
%     
%     %%% orientation Map at each scale
%     orn = zeros(size(maxImage_buffer,1),size(maxImage_buffer,2));
%     for i =1:numel(theta1)
%         orn(ind ==i)=theta1(i);
%     end
%  orientation(:,:,tau_num) = orn; 
 end