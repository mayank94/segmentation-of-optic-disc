function [centval,greenodcent]=od_center_pinpoint(rec_im,fmask1,ch,intval,majp,params)
parval=[majp(1)*500,majp(2)*500];
[hist1,cen] = hist(double(rec_im(fmask1)),[0:2:255]);
hist2=cumsum(fliplr(hist1))/sum(hist1);

x2 = find(hist2 > 0.03, 1, 'first');
x2 = cen(end-x2)+1;

Ves1= rec_im > x2;
vess=Ves1.*fmask1;
skeleton=bwmorph(vess,'skel','Inf');

skel=im2double(rec_im).*skeleton;
angles = Frangi_angles(skel);
ang=angles.*skel;
ang(ang<0)=pi+ang(ang<0);
ang(ang>pi)=ang(ang>pi)-pi;

%         ang=im2uint8(ang);
omap=ang>0&(ang<10*pi/180 | ang>170*pi/180);

if intval(1)==-1
    bcent=parval;
else
    bcent=intval;
end
odbox=make_box(bcent(1),bcent(2),60,ch);
finalimg=zeros(size(ch));
finalimg(odbox(1):odbox(2),odbox(3):odbox(4))=1;
bw=bwlabel(finalimg);
im=finalimg;

intmap=omap&im;


lb=bwlabel(intmap);
linsimage=zeros(size(ang));
midpt=[fix(size(ang,1)/2), fix(size(ang,2)/2)];
window_size=size(ang,2);
%         [ell,params,misce]=ellipse_fitting_module(skeleton1,midpt,window_size);
if sum(intmap(:))==0
   greenodcent=ch;
   linimg=[];linsimage=[];centval=[-1,-1];
   return;
end
for i=1:max(lb(:))
    fim=false(size(ang));
    fim(lb==i)=1;
    fim=fim.*double(ang);

    m=max(fim(:));
    ar(i)=m;
    [r,c]=find(fim==m,1,'first');
    for x1=1:size(ang,1)
        y1=(tan(-m)*(x1-r))+c;if y1<1 | y1>size(ang,2), continue, end
        linsimage(x1,fix(y1))=1;
    end
end
ar=[];
linimg=zeros(size(ang));
for c=1:size(ang,2)
    r=(tan(-(params(3)-(pi/2)))*(c-params(2)*500))+params(1)*500;
    linimg(fix(r),c)=1;
end
odint=im2double(linsimage)+im2double(linimg);
odpoints=odint>1;
[r,c]=find(odpoints==1);
rval=fix(median(r));
cval=fix(median(c));
centval=[rval,cval];
scrap=false(size(ch));
scrap(rval,cval)=1;
scrap=imdilate(scrap,strel('square',6));

greenodcent=ch;
greenodcent(scrap)=0;

        
        

