
function [right_val,left_val]= fitting_parabola1(outskel)
    
    imsiz = size(outskel); % nr,nc : x=r , y=c, org = topleft
    hsiz = fix(imsiz./2);
    [Y,X]=meshgrid(1:imsiz(2),1:imsiz(1));

    allpts = [X(:),Y(:)];      

     
    figure(1),
    subplot(3,2,1),
    imshow(outskel,[]), title('target')
    
    init = [hsiz./500, 10*pi/180, 500/4/500];
    
    curvepts = model_points(imsiz, init);
    initvf = parabola_function(init, allpts./500);
    
    tmp2=zeros(imsiz);
    tmp2(:)=initvf<0.02;

    figure(1)
    subplot(3,2,2), imshow(tmp2,[]), title('initialization');
    
    
    
     
    [pts_x, pts_y] = find(outskel);
    
    ptratio=fix(0.8*numel(pts_x));
    sampleidx=randsample(numel(pts_x),ptratio);
    sel_tr=pts_x(sampleidx);
    sel_tc=pts_y(sampleidx);
    sel= [sel_tr, sel_tc];
    inpts = [sel_tr, sel_tc]./500; 

    inval = zeros(size(inpts,1)+1,1);
    
    lb1 = [1/500, 1/500  , (90-10)*pi/180, 120/4/500];
    ub1 = [hsiz(1)*2/500, hsiz(2)*2/500, (90+10)*pi/180, 640/4/500]; 
%     inp=inpts*500;
%     lb1 = [1, 1 , (90-10)*pi/180, 120/4];
%     ub1 = [hsiz(1)*2, hsiz(2)*2, (90+10)*pi/180, 500/4]; 
%     init1=[hsiz, 90*pi/180, 500/4];
    [params1, resnorm1, residual1, exitflag1] = lsqcurvefit(@parabola, init, [imsiz;inpts], inval, lb1, ub1);
%     rightar=residual1(2:end)<0.013;
%     num_inliers1=sum(rightar);
    vp= parabola_function(params1, allpts./500);
    tmp3 = zeros(imsiz);
    tmp3(:)=vp<0.003;
%     right_val=[num_inliers1, params1];
    figure(1),
    subplot(3,2,3),imshow(tmp3,[]), title('fitted-right')
    
    
    lb2 = [1/500, 1/500  , (-90-10)*pi/180, 120/4/500];
    ub2 = [hsiz(1)*2/500, hsiz(2)*2/500, (-90+10)*pi/180, 640/4/500];
%     inp=inpts*500;
%     lb2 = [1, 1 , (-90-10)*pi/180, 120/4];
%     ub2 = [hsiz(1)*2, hsiz(2)*2, (-90+10)*pi/180, 500/4]; 
%     init1=[hsiz, 90*pi/180, 500/4];
        
    [params2, resnorm2, residual2, exitflag2] = lsqcurvefit(@parabola, init, [imsiz;inpts], inval, lb2, ub2);
    rightar=residual2<0.013;
    num_inliers2=sum(rightar);
    vp= parabola(params2, [imsiz; allpts./500]);
    tmp4 = zeros(imsiz);
    tmp4(:)=vp(2:end)<0.003;
    left_val=[num_inliers2, params2];
    figure(1),
    subplot(3,2,4),imshow(tmp4,[]), title('fitted-left')
    
    tmp5=tmp3+tmp4;
    figure(1),
    subplot(3,2,5),imshow(tmp5,[]), title('Double-arcade')
    
    
%     disp('vaanu')
    
%--- % % % % % % % % % % % % 

function vv = parabola_function(args,pts)

pts_x = pts(:,1)-args(1);
pts_y = pts(:,2)-args(2);

before = [pts_x, pts_y]';

c = cos(-args(3));
s = sin(-args(3));


pts_rot = [c, -s; s, c] * before;

vv = (pts_rot(2,:)).^2 - 4*args(4).*(pts_rot(1,:));
vv = abs(vv');


function pts = model_points(shape,params)

X = [0:shape(1)-1]/500;

a = params(4);

Yp = abs(sqrt(4*a*(X))); % generate parabola at 0,0
Yl = 0*X;

x0 = 0; %params(1);
y0 = 0; %params(2);

% rotate about x

c = cos(params(3));
s = sin(params(3));

pts1 = [X,X; Yp,-Yp];
% linpts1 = [X; Yl];

% pts2(1,:) = pts1(1,:)+x0;
% pts2(2,:) = pts1(2,:)+y0;
% 
% linpts2(1,:) = linpts1(1,:)+x0;
% linpts2(2,:) = linpts1(1,:)+y0;

pts3 = [c, -s; s, c]*pts1;

% linpts3 = [c, -s; s, c]*linpts1;

% translate the origin

pts4(1,:) = pts3(1,:)+x0;
pts4(2,:) = pts3(2,:)+y0;

pts = pts4';

% figure,plot(pts1(1,:),pts1(2,:),'bx')
% hold on
% plot(pts1(1,1),pts1(2,1),'gs')
% plot(pts4(1,:),pts4(2,:),'r+')
% plot(pts4(1,1),pts4(2,1),'go')
% hold off
% 
% disp('watch')


% ----------------------

function v = parabola(args,pts)

inpts = pts(2:end,:);
siz= pts(1,:);
nn=size(inpts)
args
v1 = parabola_function(args, inpts);

v=[0;v1];

return 

vessmap=zeros(siz);
for re=1:size(inpts,1)
    vessmap(inpts(re,1)*500,inpts(re,2)*500)=1;
end

[Y,X]=meshgrid(1:siz(2),1:siz(1));

allpts = [X(:),Y(:)]./500; 

vall = parabola_function(args, allpts);

parametric=zeros(siz);
parametric(:)=vall; %<0.002;
% pth = parametric<0.002;
% parskel=bwmorph(pth,'skel','Inf');
% pardist=bwdist(parskel);
[mv,ci]=min(parametric,[],2);
tmp=false(size(parametric));
for ri=1:288
    tmp(ri,ci(ri))=1;
end

vesdist=bwdist(vessmap);

% multdist=im2double(pardist).*im2double(vesdist);
distv=double(vesdist(tmp))/500;
err = sum(distv)/sum(tmp(:))

val=sqrt(err)/size(inpts,1);
% v=abs(v)'
v=[0;val.*ones(size(inpts,1),1)];
