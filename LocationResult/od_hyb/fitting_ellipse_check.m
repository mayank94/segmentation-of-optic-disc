function [ellval,resnorm1]= fitting_ellipse_check(outskel,window)
    
dbg = true;
    imsiz = size(outskel); % nr,nc : x=r , y=c, org = topleft
    hsiz = fix(imsiz./2);
    
    init = [hsiz./500, 90*pi/180, 2/3*hsiz(2)/500, 2/3*hsiz(1)/500];

    [Y,X]=meshgrid(1:imsiz(2),1:imsiz(1));

    allpts = [X(:),Y(:)];      

     
%     figure(1),
%     subplot(3,3,7),
%     imshow(outskel,[]), title('target')
    
        
     
    [pts_x, pts_y] = find(outskel);
    
    ptratio=fix(1*numel(pts_x));
    sampleidx=randsample(numel(pts_x),ptratio);
    sel_tr=pts_x(sampleidx);
    sel_tc=pts_y(sampleidx);
    sel= [sel_tr, sel_tc];
    inpts = [sel_tr, sel_tc]./500; 

    inval = zeros(size(inpts,1),1);
    
%     lb1 = [1/500, 1/500  , (90-10)*pi/180, 120/4/500];
%     ub1 = [hsiz(1)*2/500, hsiz(2)*2/500, (90+10)*pi/180, 640/4/500]; 
    lb1 = [window(1)/500, window(2)/500 , (90-10)*pi/180, 160/4/500, 160/4/500];
    ub1 = [window(3)/500, window(4)/500, (90+10)*pi/180, 700/4/500, 640/4/500]; 
%     inp=inpts*500;
%     lb1 = [1, 1 , (90-10)*pi/180, 120/4];
%     ub1 = [hsiz(1)*2, hsiz(2)*2, (90+10)*pi/180, 500/4]; 
%     init1=[hsiz, 90*pi/180, 500/4];

    if dbg

        initvf = ellipse(init, allpts./500);
        lin = find(outskel);
        tmp2=zeros(imsiz);
        tmp2(:)=initvf<0.002;
        skelinitres = initvf(lin);
        tmp2(lin)=1;
% 
        figure(1)
        subplot(3,3,4),
        num_inliers0 = sum(skelinitres<0.003);
        resnorm0 = sum(skelinitres.^2);
        imshow(tmp2,[]), title(sprintf('init inlier: %d (%.2f)',num_inliers0, resnorm0))
        %title('ellipse initializating...');
        
    end


    [params1, resnorm1, residual1, exitflag1] = lsqcurvefit(@ellipse, init, inpts, inval, lb1, ub1);
    rightar=residual1<0.003;
    num_inliers1=sum(rightar);
    vp= ellipse(params1, allpts./500);
    tmp3 = zeros(imsiz);
    tmp3(:)=vp<0.003;
    ellval=[num_inliers1, params1];
    
    if dbg
    figure(1),
    subplot(3,3,5),imshow(tmp3,[]), title(sprintf('#inlier: %d (%.2f)',num_inliers1, resnorm1))
    end
    
%     lb2 = [1/500, 1/500  , (-90-10)*pi/180, 120/4/500];
%     ub2 = [hsiz(1)*2/500, hsiz(2)*2/500, (-90+10)*pi/180, 640/4/500];
% % % % % % %     lb2 = [window(1)/500, window(2)/500 , (-90-10)*pi/180, 120/4/500, 100/4/500];
% % % % % % %     ub2 = [window(3)/500, window(4)/500, (-90+10)*pi/180, 640/4/500, 500/4/500];
% % % % % % % %     inp=inpts*500;
% % % % % % % %     lb2 = [1, 1 , (-90-10)*pi/180, 120/4];
% % % % % % % %     ub2 = [hsiz(1)*2, hsiz(2)*2, (-90+10)*pi/180, 500/4]; 
% % % % % % % %     init1=[hsiz, 90*pi/180, 500/4];
% % % % % % %         
% % % % % % %     [params2, resnorm2, residual2, exitflag2] = lsqcurvefit(@parabola, init, inpts, inval, lb2, ub2);
% % % % % % %     rightar=residual2<0.003;
% % % % % % %     num_inliers2=sum(rightar);
% % % % % % %     vp= ellipse(params2, allpts./500);
% % % % % % %     tmp4 = zeros(imsiz);
% % % % % % %     tmp4(:)=vp<0.003;
% % % % % % %     left_ellval=[num_inliers2, params2];
% % % % % % %     figure(1),
% % % % % % %     subplot(3,2,4),imshow(tmp4,[]), title('fitted-left')
    
% % % % % % %     tmp5=tmp3+tmp4;
% % % % % % %     figure(1),
% % % % % % %     subplot(3,2,5),imshow(tmp5,[]), title('Double-arcade')
% % % % % % %     
% % % % % % %     
%     disp('vaanu')
    
%--- % % % % % % % % % % % % 

function v = ellipse(args,pts)

pts_x = pts(:,1)-args(1);
pts_y = pts(:,2)-args(2);

before = [pts_x, pts_y]';



c = cos(args(3));
s = sin(args(3));


pts_rot = [c, -s; s, c]' * before;
xval=[ones(1,size(pts_rot,2))-(pts_rot(1,:).^2)/args(4)^2]*args(5)^2;

v = (pts_rot(2,:)).^2 - xval;

v=abs(v)';