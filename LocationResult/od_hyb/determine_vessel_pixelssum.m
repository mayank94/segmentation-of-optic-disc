function [halfmask,sum_vess]=determine_vessel_pixelssum(contour,linparams,vess)

bw_im=abs(contour)<0.002;
skelim=bwmorph(bw_im,'skel','Inf');

[ou,wh,direct,angles]=FrangiFilter2D(contour);

orient=tan((angles).*skelim);

% figure(5),imshow(orient,[])

x0=linparams(1);
y0=linparams(2);
slope=linparams(3);
[Y,X]=meshgrid(1:size(skelim,2),1:size(skelim,1));
    
pts=[X(:),Y(:)];

valuelin = pts(:,2)-y0-slope*(pts(:,1)-x0);
downlinimage=zeros(size(skelim));
downlinimage(:)=valuelin>0.02;
uplinimage=zeros(size(skelim));
uplinimage(:)=valuelin<0.02;

slope=-(1/slope);
valuelin = pts(:,2)-y0-slope*(pts(:,1)-x0);
leftlinimage=zeros(size(skelim));
leftlinimage(:)=valuelin<0.02;
rightlinimage=zeros(size(skelim));
rightlinimage(:)=valuelin>0.02;

[X,Y]=meshgrid(1:size(skelim,2),1:size(skelim,1));
normim=zeros(size(skelim));
pts=[X(:),Y(:)];

quadrants(:,:,1)=leftlinimage&downlinimage;
quadrants(:,:,2)=rightlinimage&downlinimage;
quadrants(:,:,3)=leftlinimage&uplinimage;
quadrants(:,:,4)=rightlinimage&uplinimage;
sum_vess=[];
norm_stack=[];

for idd = 1:4
    mask=quadrants(:,:,idd);
    quadell=skelim&mask;
    [r,c]=find(quadell==1);
    for id=1:numel(r)
    
        slope=orient(r(id),c(id));
        value = pts(:,2)-r(id)-slope*(pts(:,1)-c(id));
        normim(:)=abs(value)<5;
    %     str=num2str(slope*(180/pi));
    %     figure(2),imshow(normim),title(sprintf('%s',str));hold on;
    %     plot(c(id),r(id),'r.');
    %     hold off;
    %     pause;

%         if r(id)<size(skelim,1)
%             if c(id)<size(skelim,2)
%                 reqarea=leftlinimage&uplinimage;
%             else
%                 reqarea=uplinimage&rightlinimage;
%             end
%        else
%            if c(id)<size(skelim,2)
%                reqarea=leftlinimage&downlinimage;
%            else 
%                reqarea=downlinimage&rightlinimage;
%            end
%         end
        outim=normim&mask;

        sum_vess1(id)=sum(vess(outim));
%         figure(3),imshow(double(outim).*sum_vess1(id)/sum(outim(:))+double(skelim),[]);
%         hold on 
%         plot(c(id),r(id),'rx')
%         hold off
%         colormap jet
%         pause
        norm_stack1(:,:,id)=outim;
    end
    sum_vess=[sum_vess,sum_vess1];
    norm_stack=cat(3,norm_stack,norm_stack1);
end
[mival,mipos]=min(sum_vess);

[sval,spos]=sort(sum_vess,'ascend');
img1=zeros(size(skelim));
for k=1:fix(max(spos)/6)    
    img=norm_stack(:,:,spos(k));
    img1=img1+img;
end


lefthalf=img1&leftlinimage;
righthalf=img1&rightlinimage;

if sum(lefthalf(:))>sum(righthalf(:))
    halfmask=rightlinimage;
else
    halfmask=leftlinimage;
end

