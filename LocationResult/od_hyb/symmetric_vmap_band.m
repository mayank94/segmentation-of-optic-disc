function [vmap, maxpt,linarea1,flag1] = symmetric_vmap_band(rec_im,vesmap,acsis,fmask1, linim, linparams,hm,mav,rightval)

iacsis=acsis<1;
acsis=acsis&fmask1;
iacsis=iacsis&fmask1;
[hist1,cen] = hist(double(rec_im(acsis)),[0:2:255]);

hist2=cumsum(fliplr(hist1))/sum(hist1);
x1 = find(hist2 > 0.05, 1, 'first');
x1 = cen(end-x1)+1;
Ves= (double(rec_im).*acsis) > x1;
Ves=Ves.*fmask1;
vess1 = Ves;

[hist1,cen] = hist(double(rec_im(iacsis)),[0:2:255]);

hist2=cumsum(fliplr(hist1))/sum(hist1);
x2 = find(hist2 > 0.05, 1, 'first');
x2 = cen(end-x2)+1;
Ves= (double(rec_im).*iacsis) > x2;
Ves=Ves.*fmask1;
vess2 = Ves;

[histn,cen] = hist(double(rec_im(fmask1)),[0:2:255]);

histn=cumsum(fliplr(hist1))/sum(hist1);
x1 = find(histn > 0.03, 1, 'first');
x1 = cen(end-x1)+1;
Ves= (double(rec_im)) > x1;
Ves=Ves.*fmask1;
vess = Ves;

vmap=vess1+vess2;
lcon=bwlabel(vesmap);
reg=regionprops(lcon,'MajorAxisLength');
reg=[reg.MajorAxisLength];
poss=find(reg<20);
for f=1:length(poss)
    vesmap(lcon==poss(f))=0;
end
vmapskel=bwmorph(vmap,'skel','Inf');
vmaprem=bwmorph(vmap,'remove');
vmapdist=bwdist(vmaprem);
widthmap=2*im2double(vmapdist).*vmapskel;
filwidthmap=widthmap>(max(widthmap(:))/3);

% linim=(abs(linim)<4);
% distimg=bwdist(linimg);

[Y,X]=meshgrid(1:size(linim,2),1:size(linim,1));

    allpts = [X(:),Y(:)];    
    cenr = linparams(1);
cenc = linparams(2);
slope = linparams(3);
distim=zeros(size(linim));
% slope=tan(-slope);

distv = abs((slope*allpts(:,1)-allpts(:,2))+(cenc-slope*cenr))./hypot(slope,1);
distim(:)=distv;

focus_dist=sqrt((linparams(4)^2-linparams(5)^2));
linarea1=abs(distim)<=50;

% elinearea=imdilate(linarea1,strel('square',30));
elinearea = linarea1;
elinearea(:,fix(size(linim,2)/3):end-(fix(size(linim,2)/3)))=0;
blab=bwconncomp(elinearea);
for t=1:blab.NumObjects
    pixlist=blab.PixelIdxList{t};
    sumbox(t)=sum(filwidthmap(pixlist));
end

linarea=linarea1&hm;
cs = sum(vesmap.*linarea); % column sum
thr=0.07;
% [mv1,mi_c1]=max(cs(:,1:cenc));
% [mv2,mi_c2]=max(cs(:,cenc:end));
% mi_c2=mi_c2+cenc;
% mi_r1 = (mi_c1-cenc)/slope+cenr;
% mi_r2 = (mi_c2-cenc)/slope+cenr;
% 
% 
% dist1=hypot(cenc-mi_c1,cenr-mi_r1);
% dist2=hypot(cenc-mi_c2,cenr-mi_r2);
flag1=1;
if mav>thr
    sumreg1=sum(sum(cs(:,1:fix(size(vmap,2)/2))));
    sumreg2=sum(sum(cs(:,fix(size(vmap,2)/2):end)));
    [mv1,mi_c1]=max(cs(:,1:fix(size(vmap,2)/2)));
    [mv2,mi_c2]=max(fliplr(cs(:,fix(size(vmap,2)/2):end)));
    if sumreg1>sumreg2
        mi_c=mi_c1;
    else
        mi_c=fix(size(vmap,2)/2)+mi_c2;
    end

%     if rightval(1)>rightval(2)
%         mi_c=mi_c1;
%     else
%         mi_c=fix(size(vmap,2)/2)+mi_c2;
%     end
else
    flag1=0;
    sumreg1=sum(sum(cs(:,1:fix(size(vmap,2)/3))));
    sumreg2=sum(sum(cs(:,fix(size(vmap,2)/3):2*fix(size(vmap,2)/3))));
    sumreg3=sum(sum(cs(:,2*fix(size(vmap,2)/3):end)));
    [mv1,mi_c1]=max(cs(:,1:fix(size(vmap,2)/3)));
    [mv2,mi_c2]=max(cs(:,fix(size(vmap,2)/3):2*fix(size(vmap,2)/3)));
    [mv3,mi_c3]=max(cs(:,2*fix(size(vmap,2)/3):end));
    mval=[mv1,mv2,mv3];
    mpos=[mi_c1,fix(size(vmap,2)/3)+mi_c2,(2*fix(size(vmap,2)/3))+mi_c3];
    sval=[sumreg1,sumreg2,sumreg3];
%     sval=[rightval(3),rightval(4),rightval(5)];
    [fmv,fmp]=sort(sval,'descend');
    if mval(fmp(1))>5
        mi_c=mpos(fmp(1));
    elseif mval(fmp(2))>5
        mi_c=mpos(fmp(2));
    else
        mi_c=mpos(fmp(3));
    end
end


% if (dist1<focus_dist && dist2<focus_dist)||(dist1>focus_dist && dist2>focus_dist)
%     if max(mv1,mv2)==mv1
%         mi_c=mi_c1;
%     else
%         mi_c=mi_c2;
%     end
% elseif dist1<focus_dist && dist2>focus_dist
%     if mv2>max(mv1,mv2)/3
%         mi_c=mi_c2;
%     else
%         mi_c=mi_c1;
%     end
% elseif dist1>focus_dist && dist2<focus_dist
%     if mv1>max(mv1,mv2)/3
%         mi_c=mi_c1;
%     else
%         mi_c=mi_c2;
%     end
% end   
%     
%  [mv,mi_c]=max(cs);   


if sumbox(1)>sumbox(2) 
    if sumbox(1)-sumbox(2)>5
        sumreg1=sum(sum(cs(:,1:fix(size(vmap,2)/2))));
        sumreg2=sum(sum(cs(:,fix(size(vmap,2)/2):end)));
        [mv1,mi_c1]=max(cs(:,1:fix(size(vmap,2)/2)));
        
        mi_c=mi_c1;
    else
    sumreg1=sum(sum(cs(:,1:fix(size(vmap,2)/3))));
    sumreg2=sum(sum(cs(:,fix(size(vmap,2)/3):2*fix(size(vmap,2)/3))));
    sumreg3=sum(sum(cs(:,2*fix(size(vmap,2)/3):end)));
    [mv1,mi_c1]=max(cs(:,1:fix(size(vmap,2)/3)));
    [mv2,mi_c2]=max(cs(:,fix(size(vmap,2)/3):2*fix(size(vmap,2)/3)));
    [mv3,mi_c3]=max(cs(:,2*fix(size(vmap,2)/3):end));
    mval=[mv1,mv2,mv3];
    mpos=[mi_c1,fix(size(vmap,2)/3)+mi_c2,(2*fix(size(vmap,2)/3))+mi_c3];
    sval=[sumreg1,sumreg2,sumreg3];
%     sval=[rightval(3),rightval(4),rightval(5)];
    [fmv,fmp]=sort(sval,'descend');
    if mval(fmp(1))>5
        mi_c=mpos(fmp(1));
    elseif mval(fmp(2))>5
        mi_c=mpos(fmp(2));
    else
        mi_c=mpos(fmp(3));
    end       

    end
elseif sumbox(1)<sumbox(2) 
    
    [mv2,mi_c2]=max(cs(:,fix(size(vmap,2)/2):end));
    mi_c=fix(size(vmap,2)/2)+mi_c2;
end
    
mi_r = (mi_c-cenc)/slope+cenr;
maxpt = [mi_r, mi_c];
 thr_c1=cenc-(focus_dist/cos(1.3963-(pi/2)));
 thr_r1 = (thr_c1-cenc)/slope+cenr;
 
 thr_c2=cenc+(focus_dist/cos(1.3963-(pi/2)));
 thr_r2 = (thr_c2-cenc)/slope+cenr;
if ~true
    figure(1)
    subplot(3,4,7),imshow(vmap+linarea,[]), hold on
    plot(cenc,cenr,'b.')
    plot(mi_c,mi_r,'rs')
    plot(mi_c1,mi_r1,'g.');
    plot(mi_c2,mi_r2,'y.');
    plot(thr_c1,thr_r1,'c.');
    plot(thr_c2,thr_r2,'m.');
    hold off
end