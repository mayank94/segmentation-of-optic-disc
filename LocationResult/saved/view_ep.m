%View ellipse and parabola

%Load data
dataset = 'HRF';
data = xlsread (['Parameters,' dataset '.xls']);
addpath(genpath('../../Vaanathi/od_hyb'));
load (dataset);
[num,errors] = xlsread (['Wrongly Detected Images_' dataset '.xls']);
a=1;

for i =1:size(ODdata,1)
    filepath = ['../' dataset '/images/' ODdata{i,2}{:} '/' ODdata{i,1}];
    if ~strcmp(filepath,errors)%{1,1:size(errors,2)})
        continue;
    end
I = imread (filepath);

%Setup data
ellparams = [data(a,1)/500	data(a,2)/500  data(a,5)  data(a,3)/500	data(a,4)/500];
parparams = [data(a,8)/500	data(a,9)/500	data(a,10)	data(a,11)/500];
dloc = [data(a,6)	data(a,7)];
ellcen = [data(a,12)	data(a,13)];
parcen = [data(a,14)	data(a,15)];
imr = imresize(I,0.11,'bilinear');
[Y,X] = meshgrid(1:size(imr(:,:,1),2),1:size(imr(:,:,1),1));
allpts = [X(:),Y(:)];
outfig1=zeros(size(Y));
outfig2=zeros(size(Y));

%Generate Ellipse set
points1=ellipse(ellparams,allpts./500);
outfig1(:)=points1;
finout1=outfig1<0.001;
ellparams(1:2) = ellparams(1:2)*500;
ellparams(4:5) = ellparams(4:5)*500;
x1 = [(ellparams(2)-ellparams(4)*sin(ellparams(3))) (ellparams(2)+ellparams(4)*sin(ellparams(3)))];
y1 = [(ellparams(1)-ellparams(4)*cos(ellparams(3))) (ellparams(1)+ellparams(4)*cos(ellparams(3)))];

%Generate Parabola set
points2=parabola(parparams,allpts./500);
outfig2(:)=points2;
finout2=outfig2<0.001;
parparams(1:2) = parparams(1:2)*500;
parparams(4) = parparams(4)*500;
x2 = [parparams(2) parparams(2)+parparams(4)*sin(parparams(3))];
y2=  [parparams(1) parparams(1)+parparams(4)*cos(parparams(3))];
xline = 1:size(Y);
yline = (-tan(parparams(3))*(xline-(parparams(2)+parparams(4)*sin(parparams(3)))))+(parparams(1)+parparams(4)*cos(parparams(3)));

%Print result
figure,imshow(imr(:,:,2)+255*uint8(finout1)+255*uint8(finout2),[]);
hold on 
line(x1,y1,'Color','w');
line(x2,y2,'Color','w');
plot(xline,yline,'w');
plot(ellparams(2),ellparams(1),'bo');
plot(parparams(2),parparams(1),'go');
plot(dloc(2),dloc(1),'r.');
plot(ellcen(2),ellcen(1),'b.');
plot(parcen(2),parcen(1),'g.');
hold off;

a=a+1;
end