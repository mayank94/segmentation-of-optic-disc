dataset='CFS';
load (dataset);

[num,errors] = xlsread (['Wrongly Detected Images_' dataset '.xls']);

for ii=1:numel(errors)
    filepath = errors{ii};
    
%     [loc,fil,ext]=fileparts(filepath);
    pp=strfind(filepath,'images');
    fname = filepath(pp+7:end);
    odcen = [];
    maccen = [];
    
    for jj=1:size(ODdata,1)
        cmps=[ODdata{jj,2}{:} '/' ODdata{jj,1}];
        if strcmp(cmps, fname)
            odcen = ODdata{jj,3};
            maccen = ODdata{jj,4};
            break
        end
    end
    figure(1),imshow(filepath);
    hold on
    plot(odcen(2),odcen(1),'bs');
    plot(maccen(2),maccen(1),'wd');
    hold off
end
