dataset='STARE'; %'Kolkatta';
load (dataset);
sz=[size(ODdata,1) size(ODdata,2)];
a=1;
[num,errors] = xlsread (['Wrongly Detected Images_' dataset '.xls']);
result = [];
%Discount non-existent or erroneous

for i = 1:sz(1) %[19,36]
%      if strcmp(ODdata{i,2}{:},'Normal')
%          continue;
%      end
filepath = ['../' dataset '/images/' ODdata{i,2}{:} '/' ODdata{i,1}];
    if ~strcmp(filepath,errors)%{1,1:size(errors,2)})
        continue;
    end
img = imread (filepath);
centre = ODdata{i,3};
macula = ODdata{i,4};

addpath(genpath('../../Vaanathi/od_hyb'))
fm = im2bw(img(:,:,1),150/255);
[mask,centval, centmac,finres]=opticdisc_detection_v3(img,fm);
array = mat2cell(finres,[1],ones(1,15));
store{1} = filepath;
store(2:16) = array;
result = vertcat(result,store);
figure(8),imshow(img,[]);
title(filepath)
hold on;
plot(centre(2),centre(1),'ks','MarkerSize',20);
plot(macula(2),macula(1),'bo','MarkerSize',20);
hold off



figure(8),hold on
plot(centval(2),centval(1),'kd')
plot(centmac(2),centmac(1),'bd')
hold off
continue

%Crop image

% [m,n,~] = size(img);
% x = centre(2);
% y = centre(1);
% dim = ceil(sqrt(m*n*0.05));
% x = x-dim/2;
% y = y-dim/2;
% width = dim;
% height = dim;
% x1 = x;
% y1 = y;
% if x1<0
%     x=0;
% end
% if y1<0;
%     y=0;
% end
% if (x1+width)>n
%     width=n-x1;
% end
% if (y1+height)>m
%     height=m-y1;
% end
% rect = fix([x,y,width,height]);
% image = imcrop(img,rect);
% 
% %Morphological operation and watershed
% 
% str = strel('disk',70);
% im_open = imerode(image(:,:,1),str);
% im_reconstruct = imreconstruct(im_open,image(:,:,1));
% str = strel('disk',10);
% grad = imdilate(im_reconstruct,str)-imerode(im_reconstruct,str);
% im_wth = imtophat(grad,str);
% im_wsd = watershed(im_wth);
% colour_centre = im_wsd(round(size(im_wsd,1)/2),round(size(im_wsd,2)/2));
% properties = regionprops((im_wsd==colour_centre),'Centroid','MajorAxisLength','MinorAxisLength');

%Print Result

% figure(8),imshow(img,[]);
% hold on;
% plot(centre(2),centre(1),'rs','MarkerSize',20);
% viscircles([centre(2),centre(1)],(properties.MajorAxisLength)/2);
% hold off;

% x = input([filepath ':::']);
% if ~(isempty(x))
%     errors{a} = filepath;
%     a=a+1;
% end
end
xlswrite(strcat('Parameters,',dataset),result);